<?php
/*
Template Name: About Page
*/
?>

<?php get_header(); ?>

<main role="main">
    <div class="block block__cover block__cover--light" data-img="http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/Genteel-Black-Blazer-in-office-e1516358143414.jpg" data-imgsmall="http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/Genteel-Black-Blazer-in-office-e1516358143414.jpg">

            <div class="block__cover__content">
                <div class="block__cover__content__wrapper">
                    <div class="block__cover__title">
                        <div class="row">
                            <div class="block__cover__title__content">
                                <h1 style="color:#fff;">Our Story</h1>
                                <p class="strapline">The journey so far</p>
                                <div class="block__cover__cta scrollto" data-scrolltarget=".block-1" style="cursor:pointer;">Learn More</div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
 
        <div class="block__cover__image"></div>
    </div>
    <div class="block block__cards block__cards--mirrored block-1">
        <div class="block__cards__masked-images">
            <div class="block__cards__masked-image">
                <div class="block__cards__masked-image__container">
                    <img width="2000" height="2424" src="https://cdn.lestrangelondon.com/uploads/2016/03/story26.jpg" class="attachment-1500 size-1500" alt="" srcset="https://cdn.lestrangelondon.com/uploads/2016/03/story26.jpg 2000w, https://cdn.lestrangelondon.com/uploads/2016/03/story26-248x300.jpg 248w, https://cdn.lestrangelondon.com/uploads/2016/03/story26-768x931.jpg 768w, https://cdn.lestrangelondon.com/uploads/2016/03/story26-845x1024.jpg 845w, https://cdn.lestrangelondon.com/uploads/2016/03/story26-1485x1800.jpg 1485w, https://cdn.lestrangelondon.com/uploads/2016/03/story26-1320x1600.jpg 1320w, https://cdn.lestrangelondon.com/uploads/2016/03/story26-1155x1400.jpg 1155w, https://cdn.lestrangelondon.com/uploads/2016/03/story26-990x1200.jpg 990w, https://cdn.lestrangelondon.com/uploads/2016/03/story26-825x1000.jpg 825w, https://cdn.lestrangelondon.com/uploads/2016/03/story26-660x800.jpg 660w, https://cdn.lestrangelondon.com/uploads/2016/03/story26-528x640.jpg 528w, https://cdn.lestrangelondon.com/uploads/2016/03/story26-124x150.jpg 124w" sizes="(max-width: 2000px) 100vw, 2000px" /> </div>
            </div>
            <div class="block__cards__masked-image">
                <div class="block__cards__masked-image__container">
                    <img width="2000" height="2424" src="https://cdn.lestrangelondon.com/uploads/2016/03/story27.jpg" class="attachment-1500 size-1500" alt="" srcset="https://cdn.lestrangelondon.com/uploads/2016/03/story27.jpg 2000w, https://cdn.lestrangelondon.com/uploads/2016/03/story27-248x300.jpg 248w, https://cdn.lestrangelondon.com/uploads/2016/03/story27-768x931.jpg 768w, https://cdn.lestrangelondon.com/uploads/2016/03/story27-845x1024.jpg 845w, https://cdn.lestrangelondon.com/uploads/2016/03/story27-1485x1800.jpg 1485w, https://cdn.lestrangelondon.com/uploads/2016/03/story27-1320x1600.jpg 1320w, https://cdn.lestrangelondon.com/uploads/2016/03/story27-1155x1400.jpg 1155w, https://cdn.lestrangelondon.com/uploads/2016/03/story27-990x1200.jpg 990w, https://cdn.lestrangelondon.com/uploads/2016/03/story27-825x1000.jpg 825w, https://cdn.lestrangelondon.com/uploads/2016/03/story27-660x800.jpg 660w, https://cdn.lestrangelondon.com/uploads/2016/03/story27-528x640.jpg 528w, https://cdn.lestrangelondon.com/uploads/2016/03/story27-124x150.jpg 124w" sizes="(max-width: 2000px) 100vw, 2000px" /> </div>
            </div>
        </div>
        <div class="block__card block__card--story block-1">
            <div class="row">
                <div class="block__card__column">
                    <h3>Success today has a new look</h3>
                    <p>To us, success is never a final step; it's the onset of opportunities that we invest in. We have decided to modify the look of success, to pave the way for a superior, more culturally refined taste. By celebrating our rich cultural diversity and embracing the ever-changing way of life, we have set out to create a look that delicately balances both the past and present. </p>
                    <a class="cta cta--continue scrollto" data-scrolltarget=".block-2" href="#">Continue</a>
                </div>
            </div>
        </div>
        <div class="block__card block__card--story block-2">
            <div class="row">
                <div class="block__card__column">
                    <h3>Redefining menswear</h3>
                    <p>Men are now more conscious than ever of what reflects on the mirror and the image they project to others. From color, texture, to the detailing of the fabric much thought is expended on their dress. As the man evolves to tackle the new challenges the world presents, Genteel places itself at this nexus point with an aim of creating, “trans-leisure” by letting you transition into any setting without compromising your preferred comfort or style. It's the redefinition of a man who plays the Man, not the odds.</p>
                    <a class="cta cta--continue scrollto" data-scrolltarget=".block-3" href="#">Continue</a>
                </div>
            </div>
        </div>
    </div>
    <div class="block block__image block__image--1-up padding-bottom block-3">
        <div class="row">
            <div class="block__image__column">
                <div class="reveal--hidden fade-in-up">
                    <img width="1600" height="960" src="http://genteel.skyetechgroup.com/wp-content/uploads/2018/02/lp3.jpg" class="attachment-1600 size-1600" alt="" srcset="http://genteel.skyetechgroup.com/wp-content/uploads/2018/02/lp3.jpg 1600w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/02/lp3.jpg 300w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/02/lp3.jpg 768w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/02/lp3.jpg 1024w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/02/lp3.jpg 1400w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/02/lp3.jpg 1200w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/02/lp3.jpg 1000w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/02/lp3.jpg 800w, https://cdn.lestrangelondon.com/uploads/2016/03/story15-640x384.jpg 640w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/02/lp3.jpg 150w" sizes="(max-width: 1600px) 100vw, 1600px" /> </div>
            </div>
        </div>
    </div>
    <div class="block block__mixed story-block story-block--1">
        <div class="reveal--hidden fade-in-up story-pinned-image story-pinned-image--1">
            <div class="story-pinned-image__container" style="background-image: url('http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/Up-Magazine.jpg');"></div>
        </div>
        <div class="row">
            <div class="block__mixed__column">
                <div class="reveal--hidden fade-in">
                    <h2 class="h3 scase">Bold Inner Lining</h2>
                    <p>The intrinsic need for contextual relevance & cultural expression made us think hard of how best we are to allow the “vibe” of our fore-fathers to shine through. After a moment of enlightenment, we settled on having a bold inner lining for our suits. An embodiment of the flamboyance of Kenyan cultures and a means for self-expression.</p>
                    <a class="cta cta--continue scrollto" data-scrolltarget=".block-4" data-scrolloffset="-162" href="#">Continue</a>
                </div>
            </div>
        </div>
    </div>
    <div class="block block__mixed block-4 bg--light-grey story-block story-block--2">
        <div class="reveal--hidden fade-in-up story-pinned-image story-pinned-image--2">
            <div class="story-pinned-image__container" style="background-image: url('http://genteel.skyetechgroup.com/wp-content/uploads/2018/02/lp4.jpg');"></div>
        </div>
        <div class="row">
            <div class="block__mixed__column">
                <div class="reveal--hidden fade-in">
                    <h2 class="h3 scase">Still on the move</h2>
                    <p>Our quest to celebrate culture and redefine menswear has lead us into nothing less than an immersive interaction with the individual communities that have strived to keep their culture reasonably “unadulterated”, with a goal of curving out our cultural identity. This we call project 47 (After the 47 cultures within Kenya)- Have link to Cultural Quest. </p>
                    <a class="cta cta--continue scrollto" data-scrolltarget=".block-5" data-scrolloffset="-162" href="#">Continue</a>
                </div>
            </div>
        </div>
    </div>
    <div class="block block__image block__image--1-up block__image--1-up--right block__image--1-up--right--alt story-block story-block--3">
        <div class="row">
            <div class="block__image__column">
                <div class="reveal--hidden fade-in-up">
                    <img width="800" height="710" src="http://genteel.skyetechgroup.com/wp-content/uploads/2018/02/lp5.jpg" class="attachment-800 size-800" alt="" srcset="http://genteel.skyetechgroup.com/wp-content/uploads/2018/02/lp5.jpg 800w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/02/lp5.jpg 300w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/02/lp5.jpg 768w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/02/lp5.jpg 1024w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/02/lp5.jpg 1600w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/02/lp5.jpg 1400w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/02/lp5.jpg 1200w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/02/lp5.jpg 1000w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/02/lp5.jpg 640w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/02/lp5.jpg 150w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/02/lp5.jpg 500w" sizes="(max-width: 800px) 100vw, 800px" /> </div>
            </div>
        </div>
    </div>
    <div class="block block__image block__image--1-up no-padding-bottom story-block story-block--4 block-5" data-fill-edge="top">
        <div class="row">
            <div class="block__image__column">
                <img width="1600" height="960" src="http://genteel.skyetechgroup.com/wp-content/uploads/2017/07/ADS_1058.jpg" class="reveal--hidden fade-in-up" alt="" srcset="http://genteel.skyetechgroup.com/wp-content/uploads/2017/07/ADS_1058.jpg 1600w, http://genteel.skyetechgroup.com/wp-content/uploads/2017/07/ADS_1058.jpg 300w, http://genteel.skyetechgroup.com/wp-content/uploads/2017/07/ADS_1058.jpg 768w, http://genteel.skyetechgroup.com/wp-content/uploads/2017/07/ADS_1058.jpg 1024w, http://genteel.skyetechgroup.com/wp-content/uploads/2017/07/ADS_1058.jpg 1400w, http://genteel.skyetechgroup.com/wp-content/uploads/2017/07/ADS_1058.jpg 1200w, http://genteel.skyetechgroup.com/wp-content/uploads/2017/07/ADS_1058.jpg 1000w, http://genteel.skyetechgroup.com/wp-content/uploads/2017/07/ADS_1058.jpg 800w, http://genteel.skyetechgroup.com/wp-content/uploads/2017/07/ADS_1058.jpg 640w, http://genteel.skyetechgroup.com/wp-content/uploads/2017/07/ADS_1058.jpg 150w" sizes="(max-width: 1600px) 100vw, 1600px" />
                <div class="text--time reveal--hidden fade-in-up">13<span>:</span>01</div>
            </div>
        </div>
        <div class="fill--light-grey fill--light-grey--half"></div>
    </div>
    <div class="block block__mixed bg--light-grey story-block story-block--5">
        <div class="story-pinned-image story-pinned-image--3">
            <div class="story-pinned-image__container reveal--hidden fade-in-up" style="background-image: url('http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/abt.jpg');"></div>
            <div class="text--time reveal--hidden fade-in-up">7<span>:</span>05</div>
        </div>
        <div class="row">
            <div class="block__mixed__column">
                <div class="reveal--hidden fade-in">
                    <h2 class="h3 scase">Tailoring the man on the move</h2>
                    <p>Our collection of key transitional pieces takes you all the way through the day and into the evening. </p>
                    <a class="cta cta--continue scrollto" data-scrolltarget=".block-6" href="#">Continue</a>
                </div>
            </div>
        </div>
    </div>
    <div class="block block__image block__image--2-up bg--light-grey story-block story-block--6">
        <div class="row">
            <div class="block__image__column story-offset--2">
                <img width="800" height="710" src="http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/abt2.jpg" class="reveal--hidden fade-in-up" alt="" srcset="http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/abt2.jpg 800w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/abt2.jpg 300w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/abt2.jpg 768w,http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/abt2.jpg 1024w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/abt2.jpg 1600w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/abt2.jpg 1400w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/abt2.jpg 1200w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/abt2.jpg 1000w,http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/abt2.jpg 640w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/abt2.jpg 150w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/abt2.jpg 500w" sizes="(max-width: 800px) 100vw, 800px" />
                <div class="text--time reveal--hidden fade-in-up">6<span>:</span>45</div>
            </div>
            <div class="block__image__column">
                <img width="660" height="800" src="http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/abt3.jpg" class="reveal--hidden fade-in-up" alt="" srcset="http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/abt3.jpg 660w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/abt3.jpg 248w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/abt3.jpg 768w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/abt3.jpg 845w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/abt3.jpg 1485w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/abt3.jpg 1320w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/abt3.jpg 1155w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/abt3.jpg 990w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/abt3.jpg 825w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/abt3.jpg 528w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/abt3.jpg 124w" sizes="(max-width: 660px) 100vw, 660px" />
                <div class="text--time reveal--hidden fade-in-up">18<span>:</span>46</div>
            </div>
        </div>
    </div>
    <!--<div class="block block__image-text bg--dark block-6" data-img="https://cdn.lestrangelondon.com/uploads/2016/03/Storyretail1-1.jpg" data-imgsmall="https://cdn.lestrangelondon.com/uploads/2016/03/Storyretail1-1.jpg">-->
    <!--    <div class="row">-->
    <!--        <div class="block__image-text__column">-->
    <!--            <h2 style="color:#fff;">Our retail spaces</h2>-->
    <!--            <p>Our stores give us the opportunity to showcase our latest collections and introduce L’Estrange to a new audience. We believe it is important to have a physical space for people to appreciate the quality of the garments we produce and understand what makes them special.</p>-->
    <!--            <a class="cta cta--continue scrollto" data-scrolltarget=".block-7" href="#">Continue</a>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--    <div class="block__image-text__image"></div>-->
    <!--</div>-->
    <div class="block block__text bg--light-grey block-7 story-block--7">
        <div class="row">
            <div class="block__text__column">
                <h2 class="h2--larger no-margin-bottom">The Founders</h2>
            </div>
        </div>
    </div>
    <div class="block block__image block__image--1-up block__image--1-up--narrow bg--light-grey block__image--tighter">
        <div class="row">
            <div class="block__image__column reveal--hidden fade-in-up">
                <div class="ui__blend--static">
                    <img width="800" height="710" src="http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/BP2O2299.jpg" class="attachment-800 size-800" alt="" srcset="http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/BP2O2299.jpg 800w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/BP2O2299.jpg 300w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/BP2O2299.jpg 768w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/BP2O2299.jpg 1024w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/BP2O2299.jpg 1600w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/BP2O2299.jpg 1400w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/BP2O2299.jpg 1200w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/BP2O2299.jpg 1000w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/BP2O2299.jpg 640w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/BP2O2299.jpg 150w, http://genteel.skyetechgroup.com/wp-content/uploads/2018/01/BP2O2299.jpg 500w" sizes="(max-width: 800px) 100vw, 800px" /> </div>
            </div>
        </div>
    </div>
    <div class="block block__quote block__quote--pull-quote bg--light-grey story-block--8">
        <div class="row">
            <blockquote class="block__quote__column block__quote__column--narrow">
                <p class="text--large">Image Is Half The Story Told.</p>
                <cite>
                    <a href="http://genteel.skyetechgroup.com/about/sam-omindo">SAM</a> & <a href="http://genteel.skyetechgroup.com/about/brian-balyach">BRIAN</a> <span>GENTEEL</span>
                </cite>
            </blockquote>
        </div>
    </div>
   
</main>

<?php get_footer('custom3'); ?>