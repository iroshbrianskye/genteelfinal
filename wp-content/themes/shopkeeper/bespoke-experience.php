<?php /* Template Name: BespokeExperience */ ?>

<?php get_header(); ?>

 <main>
 
        <section class="hero rr-section" style="background-color:#332E31;height:87vh;">
            <div class="oc middle left">
                <div class="container">
                    <figure class="adaptive-image ">
                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/00.hero.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/00.hero.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/00.hero.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/00.hero.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/00.hero.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/00.hero.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/00.hero.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/00.hero.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/00.hero.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 100vw,
                    (min-width: 1024px) 100vw,
                    (min-width: 768px) 100vw,
                    (min-width: 599px) 100vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="00.hero" />
                    </figure>
                </div>
            </div>
            <div class="inner maintext">
                <span class="colourwash" style="background-color:#332E31;"></span>
                <h1>INDIVIDUAL EXPRESSIONS</h1>
                <div class="children">
                </div>
            </div>
        </section>
        <section style="background-color:;" class="rr-section     ">
            <div class="bg">
            </div>
            <div class="children">
                <div class="headline ">
                    <article>
                        <div class="introtext h4 ">
                            <p>Each client has his own individual style that acts as the starting point for his bespoke garments. Measurements are keenly taken by our tailor, who will also take in to account all the characteristics of the individual’s physique such as posture, the roll of the shoulders and any minute variations in the lengths of the arms. The measurements are used to create the pattern, which is cut by hand. The pattern will become the personal template for what we make up for the client. The process between taking of measurements and having the complete garment takes about 5 weeks. 
                                <br />
                                <br /> Discover the full process. </p>
                        </div>
                    </article>
                </div>
                <div class="cards ">
                    <div class="cards-holder single">
                        <figure class="adaptive-image ">
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/02.16x9_12-column-image.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/02.16x9_12-column-image.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/02.16x9_12-column-image.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/02.16x9_12-column-image.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/02.16x9_12-column-image.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/02.16x9_12-column-image.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/02.16x9_12-column-image.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/02.16x9_12-column-image.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/02.16x9_12-column-image.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 1160px,
                    (min-width: 1024px) 1160px,
                    (min-width: 768px) 100vw,
                    (min-width: 599px) 100vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="02.16x9_12-column-image" />
                        </figure>
                    </div>
                </div>
                <div class="cards two">
                    <div class="cards-holder left-column">
                        <figure class="adaptive-image ">
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_clock.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_clock.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_clock.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_clock.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_clock.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_clock.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_clock.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_clock.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_clock.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 570px,
                    (min-width: 1024px) 570px,
                    (min-width: 768px) 50vw,
                    (min-width: 599px) 50vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="06.4x3_fullbleed-image_clock" />
                        </figure>
                    </div>
                    <div class="cards-holder">
                        <figure class="adaptive-image ">
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_v2.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_v2.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_v2.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_v2.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_v2.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_v2.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_v2.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_v2.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_v2.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 570px,
                    (min-width: 1024px) 570px,
                    (min-width: 768px) 50vw,
                    (min-width: 599px) 50vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="06.4x3_fullbleed-image_v2" />
                        </figure>
                    </div>
                </div>
                <div class="headline ">
                    <article>
                        <h2 style="color:;  ">Cloth Selection and cutting</h2>
                        <div class="introtext h4 ">
                            <p>We offer an extensive range of bespoke materials from which our clients can choose from. With the help of our cutter, the client selects the cloth, keeping in mind the particular style envisioned.
When the cloth is delivered to our workshops, the pattern is laid out on the cloth and marked by chalk. The cloth is then cut by hand, creating all the individual panels for the garment. At this stage, our cutters purposely leave extra cloth at the inlays to allow the suit to be altered at a future date should the client’s weight change. That is because a Genteel suit is an investment—one that we expect to be of tremendous quality, lasting for extensive periods of time.
</p>
                        </div>
                    </article>
                </div>
                <figure class="adaptive-image ">
                    <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03.16x9_fullbleed-image.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03.16x9_fullbleed-image.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03.16x9_fullbleed-image.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03.16x9_fullbleed-image.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03.16x9_fullbleed-image.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03.16x9_fullbleed-image.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03.16x9_fullbleed-image.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03.16x9_fullbleed-image.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03.16x9_fullbleed-image.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 100vw,
                    (min-width: 1024px) 100vw,
                    (min-width: 768px) 100vw,
                    (min-width: 599px) 100vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="03.16x9_fullbleed-image" />
                </figure>
                <div class="headline ">
                    <article>
                        <h2 style="color:;  ">Trim</h2>
                        <div class="introtext h4 ">
                            <p>Once the cloth has been cut to shape, creating the foundation of the suit, additional materials are assembled such as our signature inner linings. The materials are sorted by hand to ensure quality control, checking for quality, strength and appearance.
                                <br />
                                <br /> 
</p>
                        </div>
                    </article>
                </div>
                <div class="cards ">
                    <div class="cards-holder single">
                        <figure class="adaptive-image ">
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images//F.3.5_Bespoke_Ghost_03_grid_v1.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images//F.3.5_Bespoke_Ghost_03_grid_v1.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images//F.3.5_Bespoke_Ghost_03_grid_v1.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images//F.3.5_Bespoke_Ghost_03_grid_v1.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images//F.3.5_Bespoke_Ghost_03_grid_v1.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images//F.3.5_Bespoke_Ghost_03_grid_v1.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images//F.3.5_Bespoke_Ghost_03_grid_v1.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images//F.3.5_Bespoke_Ghost_03_grid_v1.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images//F.3.5_Bespoke_Ghost_03_grid_v1.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 1160px,
                    (min-width: 1024px) 1160px,
                    (min-width: 768px) 100vw,
                    (min-width: 599px) 100vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="F.3.5_Bespoke_Ghost_03_grid_v1" />
                        </figure>
                    </div>
                </div>
                <div class="cards two">
                    <div class="cards-holder left-column">
                        <figure class="adaptive-image ">
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_column-image.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_column-image.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_column-image.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_column-image.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_column-image.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_column-image.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_column-image.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_column-image.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_column-image.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 570px,
                    (min-width: 1024px) 570px,
                    (min-width: 768px) 50vw,
                    (min-width: 599px) 50vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="06.4x3_column-image" />
                        </figure>
                    </div>
                    <div class="cards-holder">
                        <figure class="adaptive-image ">
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_05_grid_v1.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_05_grid_v1.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_05_grid_v1.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_05_grid_v1.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_05_grid_v1.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_05_grid_v1.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_05_grid_v1.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_05_grid_v1.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_05_grid_v1.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 570px,
                    (min-width: 1024px) 570px,
                    (min-width: 768px) 50vw,
                    (min-width: 599px) 50vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="F.3.5_Bespoke_Ghost_05_grid_v1" />
                        </figure>
                    </div>
                </div>
                <div class="headline ">
                    <article>
                        <h2 style="color:;  ">Putting Together</h2>
                        <div class="introtext h4 ">
                            <p>Each client gets his own highly skilled and experienced tailor, who knows from experience how best to make up that client’s clothes. After the trim has been added to the suit, the customer’s garment is assigned to his tailor, who will use his well-trained eye to observe the pattern for the smallest imperfections. Only when he is entirely satisfied does he begin sewing it together. This is done by hand and readied for the first fitting.</p>
                        </div>
                    </article>
                </div>
                <figure class="adaptive-image ">
                    <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_06_fullbleedpair_v1.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_06_fullbleedpair_v1.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_06_fullbleedpair_v1.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_06_fullbleedpair_v1.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_06_fullbleedpair_v1.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_06_fullbleedpair_v1.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_06_fullbleedpair_v1.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_06_fullbleedpair_v1.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_06_fullbleedpair_v1.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 100vw,
                    (min-width: 1024px) 100vw,
                    (min-width: 768px) 100vw,
                    (min-width: 599px) 100vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="F.3.5_Bespoke_Ghost_06_fullbleedpair_v1" />
                </figure>
                <div class="headline ">
                    <article>
                        <h2 style="color:;  ">First Fitting</h2>
                        <div class="introtext h4 ">
                            <p>
During the first fitting, the cutter begins to make alterations to the suit according to the client’s posture.
<br><br> </p>
<h2 style="color:;  ">Marking Up</h2>
<p>After the first fitting, the garment is completely taken apart, pressed and re-cut, taking in to consideration any necessary adjustments. It is an ever changing process, time-consuming and done by hand, but one that assures that the client gets what he envisioned even before his first visit.
<br>
Once marking up has been done to the cutter’s satisfaction, the suit is given back to the tailor to be prepared for the second fitting. The client’s assigned tailor will then again observe the pattern for any imperfections before the suit is canvassed by hand ready, for the client’s next visit.
</p>
                        </div>
                    </article>
                </div>
                <div class="cards ">
                    <div class="cards-holder single">
                        <figure class="adaptive-image ">
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/SOE.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/SOE.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/SOE.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/SOE.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/SOE.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/SOE.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/SOE.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/SOE.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/SOE.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 1160px,
                    (min-width: 1024px) 1160px,
                    (min-width: 768px) 100vw,
                    (min-width: 599px) 100vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="SOE" />
                        </figure>
                    </div>
                </div>
                <div class="cards two">
                    <div class="cards-holder left-column">
                        <figure class="adaptive-image ">
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/EXTERIOR.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/EXTERIOR.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/EXTERIOR.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/EXTERIOR.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/EXTERIOR.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/EXTERIOR.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/EXTERIOR.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/EXTERIOR.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/EXTERIOR.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 570px,
                    (min-width: 1024px) 570px,
                    (min-width: 768px) 50vw,
                    (min-width: 599px) 50vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="EXTERIOR" />
                        </figure>
                    </div>
                    <div class="cards-holder">
                        <figure class="adaptive-image ">
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/INTERIOR.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/INTERIOR.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/INTERIOR.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/INTERIOR.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/INTERIOR.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/INTERIOR.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/INTERIOR.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/INTERIOR.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/INTERIOR.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 570px,
                    (min-width: 1024px) 570px,
                    (min-width: 768px) 50vw,
                    (min-width: 599px) 50vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="INTERIOR" />
                        </figure>
                    </div>
                </div>
<div class="headline ">
                    <article>
                        <h2 style="color:;  ">Second Fitting</h2>
                        <div class="introtext h4 ">
                            <p>Our bespoke process ensures that we carefully examine the garment during the fittings, looking for any imperfections to the final finished in which the suit will eventually fit the client.
 <br><br>
During the second fitting, the cutter will check the previous alterations from the first fitting, all with an eye toward making all required further adjustments and refinements.
Next the specialist features of the suit are examined by the cutter, inspecting the break over shoe, the seat of trouser, the drape, neck point and cuffs, to name only a few key points of inspection.
</p>
                        </div>
                    </article>
                </div>

            <div class="cards ">
                    <div class="cards-holder single">
                        <figure class="adaptive-image ">
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images//F.3.5_Bespoke_Ghost_03_grid_v1.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images//F.3.5_Bespoke_Ghost_03_grid_v1.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images//F.3.5_Bespoke_Ghost_03_grid_v1.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images//F.3.5_Bespoke_Ghost_03_grid_v1.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images//F.3.5_Bespoke_Ghost_03_grid_v1.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images//F.3.5_Bespoke_Ghost_03_grid_v1.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images//F.3.5_Bespoke_Ghost_03_grid_v1.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images//F.3.5_Bespoke_Ghost_03_grid_v1.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images//F.3.5_Bespoke_Ghost_03_grid_v1.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 1160px,
                    (min-width: 1024px) 1160px,
                    (min-width: 768px) 100vw,
                    (min-width: 599px) 100vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="F.3.5_Bespoke_Ghost_03_grid_v1" />
                        </figure>
                    </div>
                </div>
                <div class="cards two">
                    <div class="cards-holder left-column">
                        <figure class="adaptive-image ">
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_column-image.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_column-image.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_column-image.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_column-image.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_column-image.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_column-image.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_column-image.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_column-image.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_column-image.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 570px,
                    (min-width: 1024px) 570px,
                    (min-width: 768px) 50vw,
                    (min-width: 599px) 50vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="06.4x3_column-image" />
                        </figure>
                    </div>
                    <div class="cards-holder">
                        <figure class="adaptive-image ">
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_05_grid_v1.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_05_grid_v1.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_05_grid_v1.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_05_grid_v1.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_05_grid_v1.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_05_grid_v1.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_05_grid_v1.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_05_grid_v1.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.5_Bespoke_Ghost_05_grid_v1.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 570px,
                    (min-width: 1024px) 570px,
                    (min-width: 768px) 50vw,
                    (min-width: 599px) 50vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="F.3.5_Bespoke_Ghost_05_grid_v1" />
                        </figure>
                    </div>
                </div>

                <div class="headline ">
                    <article>
                        <h2 style="color:;  ">Finishing</h2>
                        <div class="introtext h4 ">
                            <p>Once again, the suit is dismantled in order to make the final alterations. The suit is at long last ready to be put back together and finished by hand. The collar and sleeves and buttons are sewn in, and the felling and edge stitching is completed
 <br><br>
As the tailor finishes the suit inside and out, he creates a one-of-a -kind garment that gives a unique look to every client.
 <br><br>
All finishing is done at the Genteel premises, where the client will return to see the finished suit for the first time at the imperative final fitting.

</p>
                        </div>
                    </article>
                </div>
        
                <section class="ctacontainer">
                    <div class="inner">
                        <span class="cta   primary">
    <a href="#" target="_self" data-action="default" class="h5 ">Discover Ghost</a>
</span>
                        <span class="cta  ">
    <a target="_blank" data-action="default" class="h5 ">Ask a question</a>
</span>
                    </div>
                </section>
            </div>
        </section>
        <section style="background-color:;" class="rr-section     ">
            <div class="bg">
                <figure class="adaptive-image ">
                    <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/section1.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/section1.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/section1.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/section1.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/section1.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/section1.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/section1.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/section1.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/section1.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 100vw,
                    (min-width: 1024px) 100vw,
                    (min-width: 768px) 100vw,
                    (min-width: 599px) 100vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="section1" />
                </figure>
            </div>
            <div class="children">
                <div class="headline ">
                    <article>
                        <h2 class="text-center" style="color:;  ">Continue your journey</h2>
                    </article>
                </div>
                <div class="cards two">
                    <div class="cards-holder left-column">
                        <article class="js-expandable card">
                            <a href="#">
        <span class="image">

        <figure class="adaptive-image ">
            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/01.4x3_card.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/01.4x3_card.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/01.4x3_card.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/01.4x3_card.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/01.4x3_card.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/01.4x3_card.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/01.4x3_card.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/01.4x3_card.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/01.4x3_card.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 570px,
                    (min-width: 1024px) 570px,
                    (min-width: 768px) 50vw,
                    (min-width: 599px) 50vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="01.4x3_card"/>
            
        </figure>

        </span>
        <span class="text">
            <h3>Wraith: unparalleled personalisation</h3>
            <p>Our Bespoke team go to great lengths to make your <span style="white-space: nowrap;display: inline;padding: 0;">Rolls-Royce </span>
exactly how you envisage it. Explore some of our finest commissioned Wraith motor cars.</p>
        </span>
    </a>
                        </article>
                    </div>
                    <div class="cards-holder">
                        <article class="js-expandable card">
                            <a href="#">
        <span class="image">
 
        <figure class="adaptive-image ">
            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.4_Thewoodshop_00_card_v1.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.4_Thewoodshop_00_card_v1.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.4_Thewoodshop_00_card_v1.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.4_Thewoodshop_00_card_v1.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.4_Thewoodshop_00_card_v1.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.4_Thewoodshop_00_card_v1.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.4_Thewoodshop_00_card_v1.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.4_Thewoodshop_00_card_v1.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/F.3.4_Thewoodshop_00_card_v1.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 570px,
                    (min-width: 1024px) 570px,
                    (min-width: 768px) 50vw,
                    (min-width: 599px) 50vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="F.3.4_Thewoodshop_00_card_v1"/>
            
        </figure>

        </span>
        <span class="text">
            <h3>Patience. Precision. Perfection.</h3>
            <p>
Our specialists handpick and craft every single piece of wood used in a <span style="white-space: nowrap;display: inline;padding: 0;">Rolls-Royce </span>
motor car. We go inside the Woodshop and discover the story behind the world’s finest marquetry.</p>
        </span>
    </a>
                        </article>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <script>
    var rrConfig = { "forms": "/bin/rollsroyce/sf/create.json", "configuratorUrl": "configure-12.rolls-roycemotorcars.com/start_intern_nf.html", "dealerSearch": "/services/dealerlocator", "countryList": "/services/dealerlocator/countries", "dealerByCountry": "/services/dealerlocator/country", "siteSearch": "/services/gsa", "googleApiKey": "AIzaSyD95XTWgCi1KsIeZZcIZhyWvI0DAsHkqEY", "vehicleRecall": "undefined" };
    </script>
    <div class="shade"></div>



<?php get_footer(); ?>

