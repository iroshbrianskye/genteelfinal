<?php /* Template Name: Bespoke */ ?>

<?php get_header(); ?>

<main>
        <section class="hero rr-section" style="background-color:#332E31;">
            <div class="oc middle left">
                <div class="container">
                    <figure class="adaptive-image ">
                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/08.card_2x1_BB-ghost.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/08.card_2x1_BB-ghost.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/08.card_2x1_BB-ghost.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/08.card_2x1_BB-ghost.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/08.card_2x1_BB-ghost.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/08.card_2x1_BB-ghost.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/08.card_2x1_BB-ghost.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/08.card_2x1_BB-ghost.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/08.card_2x1_BB-ghost.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 1160px,
                    (min-width: 1024px) 1160px,
                    (min-width: 768px) 100vw,
                    (min-width: 599px) 100vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="08.card_2x1_BB-ghost"/>
                    </figure>
                </div>
            </div>
            <div class="inner maintext" style="position:absolute;left:32%;">
                <span class="colourwash" style="background-color:#332E31;"></span>
                <h1 style="text-align:center;">DRESS LIKE A BOSS</h1>
                <div class="children">
                </div>
            </div>
        </section>
        <section style="background-color:;" class="rr-section     ">
            <div class="bg">
                <figure class="adaptive-image ">
                    <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/01.FAMILY_backplate1.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/01.FAMILY_backplate1.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/01.FAMILY_backplate1.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/01.FAMILY_backplate1.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/01.FAMILY_backplate1.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/01.FAMILY_backplate1.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/01.FAMILY_backplate1.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/01.FAMILY_backplate1.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/01.FAMILY_backplate1.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 100vw,
                    (min-width: 1024px) 100vw,
                    (min-width: 768px) 100vw,
                    (min-width: 599px) 100vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="01.FAMILY_backplate1" />
                </figure>
            </div>
            <div class="children">
                <div class="headline ">
                    <article>
                        <h2 style="color:;  ">When normal business<br />
hours do not apply</h2>
                        <div class="introtext h4 ">
                            <p>The suit is for life and not tied down to the office space or season. It is made suitable for different localities and occasions, ensuring a clean professional look, each and every single time. Bespoke suits are fit for the ever changing seasons of life, exclusively hand crafted for those who can hear tomorrow coming. </p>
                        </div>
                    </article>
                </div>
                <div>
                    <div class="unobtrusive dynamic-cta-wrapper" data-case="default">
                        <section class="ctacontainer">
                            <div class="inner">
                                <span class="cta   primary">
    <a target="_blank" data-action="default" class="h5 ">Contact Dealer</a>
</span>
                                <span class="cta  ">
    <a target="_blank" data-action="default" class="h5 ">Make an Enquiry</a>
</span>
                            </div>
                        </section>
                    </div>
                    <div class="unobtrusive dynamic-cta-wrapper" data-case="cid">
                        <section class="ctacontainer">
                            <div class="inner">
                                <span class="cta   primary">
    <a target="_blank" data-action="default" class="h5 ">Contact Dealer</a>
</span>
                                <span class="cta  ">
    <a target="_blank" data-action="default" class="h5 ">Make an Enquiry</a>
</span>
                            </div>
                        </section>
                    </div>
                    <div class="unobtrusive dynamic-cta-wrapper" data-case="lid">
                        <section class="ctacontainer">
                            <div class="inner">
                            </div>
                        </section>
                    </div>
                </div>
                <div class="spacer">
                </div>
                <div class="cards ">
                    <div class="cards-holder single">
                        <figure class="adaptive-image ">
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/02.FAMILY_16x9_12-column-image.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/02.FAMILY_16x9_12-column-image.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/02.FAMILY_16x9_12-column-image.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/02.FAMILY_16x9_12-column-image.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/02.FAMILY_16x9_12-column-image.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/02.FAMILY_16x9_12-column-image.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/02.FAMILY_16x9_12-column-image.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/02.FAMILY_16x9_12-column-image.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/02.FAMILY_16x9_12-column-image.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 1160px,
                    (min-width: 1024px) 1160px,
                    (min-width: 768px) 100vw,
                    (min-width: 599px) 100vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="02.FAMILY_16x9_12-column-image" />
                        
                        </figure>
                    </div>
                </div>
                <div class="cards two">
                    <div class="cards-holder left-column">
                        <figure class="adaptive-image ">
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03.FAMILY_4x3-over-16x9_12-column-image-grid.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03.FAMILY_4x3-over-16x9_12-column-image-grid.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03.FAMILY_4x3-over-16x9_12-column-image-grid.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03.FAMILY_4x3-over-16x9_12-column-image-grid.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03.FAMILY_4x3-over-16x9_12-column-image-grid.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03.FAMILY_4x3-over-16x9_12-column-image-grid.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03.FAMILY_4x3-over-16x9_12-column-image-grid.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03.FAMILY_4x3-over-16x9_12-column-image-grid.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03.FAMILY_4x3-over-16x9_12-column-image-grid.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 570px,
                    (min-width: 1024px) 570px,
                    (min-width: 768px) 50vw,
                    (min-width: 599px) 50vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="03.FAMILY_4x3-over-16x9_12-column-image-grid" />
                        </figure>
                    </div>
                    <div class="cards-holder">
                        <figure class="adaptive-image ">
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/04.FAMILY_4x3-over-16x9_12-column-image-grid.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/04.FAMILY_4x3-over-16x9_12-column-image-grid.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/04.FAMILY_4x3-over-16x9_12-column-image-grid.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/04.FAMILY_4x3-over-16x9_12-column-image-grid.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/04.FAMILY_4x3-over-16x9_12-column-image-grid.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/04.FAMILY_4x3-over-16x9_12-column-image-grid.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/04.FAMILY_4x3-over-16x9_12-column-image-grid.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/04.FAMILY_4x3-over-16x9_12-column-image-grid.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/04.FAMILY_4x3-over-16x9_12-column-image-grid.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 570px,
                    (min-width: 1024px) 570px,
                    (min-width: 768px) 50vw,
                    (min-width: 599px) 50vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="04.FAMILY_4x3-over-16x9_12-column-image-grid" />
                        </figure>
                    </div>
                </div>
                
                <div class="spacer">
                </div>
            </div>
        </section>
        <section style="background-color:;" class="rr-section     ">
            <div class="bg">
            </div>
            <div class="children">
                <div class="headline ">
                    <article>
                        <h2 style="color:;  ">Understated Power</h2>
                        <div class="introtext h4 ">
                            <p>Real luxury is understanding quality, and having the time to enjoy it. Besides the suit’s exquisite form and its precise fit, goes hand in hand sense of power that surges from within. With its precise detailing and clean finishes, this suit is carefully engineered to make a grand statement that lasts a lifetime.</p>
                        </div>
                        <div class="paragraph js-expandable expand">
                            <a href="#" class="expander" data-action="expand"></a>
                            <div class="js-expand expandable">
                                <div>
                                    <p>Fluid lines, subtle sculpting and the presence of the iconic <span style="white-space: nowrap;display: inline;padding: 0;">Rolls-Royce </span>grille - every detail is precisely engineered for an effortless driving experience whenever and wherever your world takes you. </p>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <section class="ctacontainer">
                    <div class="inner">
                        <span class="cta   primary">
    <a target="_blank" data-action="default" class="h5 " id="rsvp">RSVP FOR APPOINTMENT</a>
</span>
                    </div>
                </section>
                <div class="spacer">
                </div>
                <figure class="adaptive-image ">
                    <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/05.FAMILY_fullbleed-grid.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/05.FAMILY_fullbleed-grid.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/05.FAMILY_fullbleed-grid.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/05.FAMILY_fullbleed-grid.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/05.FAMILY_fullbleed-grid.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/05.FAMILY_fullbleed-grid.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/05.FAMILY_fullbleed-grid.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/05.FAMILY_fullbleed-grid.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/05.FAMILY_fullbleed-grid.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 100vw,
                    (min-width: 1024px) 100vw,
                    (min-width: 768px) 100vw,
                    (min-width: 599px) 100vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="05.FAMILY_fullbleed-grid" />
                </figure>
            </div>
        </section>
        <section style="background-color:;" class="rr-section     ">
            <div class="bg">
                <figure class="adaptive-image ">
                    <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.FAMILY_backplate2.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.FAMILY_backplate2.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.FAMILY_backplate2.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.FAMILY_backplate2.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.FAMILY_backplate2.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.FAMILY_backplate2.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.FAMILY_backplate2.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.FAMILY_backplate2.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.FAMILY_backplate2.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 100vw,
                    (min-width: 1024px) 100vw,
                    (min-width: 768px) 100vw,
                    (min-width: 599px) 100vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="06.FAMILY_backplate2" />
                </figure>
            </div>
            <div class="children">
                <div class="headline ">
                    <article>
                        <h2 style="color:#FFFFFF;  ">See our attention to detail</h2>
                        <div class="introtext h4 " style="color:#FFFFFF;">
                            <p>Each bespoke suit here at Genteel is hand crafted according to your specifications, hence turning your dreams into reality. This gives you the unique opportunity to dress according to what you envision for your outfits, to suit the ever changing seasons of life.</p>
                        </div>
                        <div class="paragraph js-expandable expand" style="color:#FFFFFF;">
                            <a href="#" class="expander" data-action="expand"></a>
                            <div class="js-expand expandable">
                                <div>
                                    <p>Bespoke is about more than a distinctive design. It’s about bringing your vision to life – a vision only limited by your imagination. From the finest detailing to the most extravagant statements, collaborate with our Bespoke team and make your dream Ghost a reality.</p>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <section class="ctacontainer">
                    <div class="inner">
                        <span class="cta   primary">
    <a href="/experience-bespoke" target="_self" data-action="default" class="h5 ">Explore bespoke</a>
</span>
                    </div>
                </section>
                <div class="spacer">
                </div>
                <div class="cards ">
                    <div class="cards-holder single">
                        <figure class="adaptive-image ">
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/07.FAMILY_16x9_12-column-image.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/07.FAMILY_16x9_12-column-image.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/07.FAMILY_16x9_12-column-image.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/07.FAMILY_16x9_12-column-image.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/07.FAMILY_16x9_12-column-image.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/07.FAMILY_16x9_12-column-image.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/07.FAMILY_16x9_12-column-image.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/07.FAMILY_16x9_12-column-image.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/07.FAMILY_16x9_12-column-image.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 1160px,
                    (min-width: 1024px) 1160px,
                    (min-width: 768px) 100vw,
                    (min-width: 599px) 100vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="07.FAMILY_16x9_12-column-image" />
                        </figure>
                    </div>
                </div>
                <div class="cards two">
                    <div class="cards-holder left-column">
                        <figure class="adaptive-image ">
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
               http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_clock.jpg.rr.640.LOW.jpg 320w,
               http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_clock.jpg.rr.720.LOW.jpg 360w,
               http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_clock.jpg.rr.1198.LOW.jpg 599w,
               http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_clock.jpg.rr.1536.LOW.jpg 768w,
               http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_clock.jpg.rr.1600.LOW.jpg 800w,
               http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_clock.jpg.rr.2048.LOW.jpg 1024w,
               http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_clock.jpg.rr.1160.MED.jpg 1160w,
               http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_clock.jpg.rr.1366.MED.jpg 1366w,
               http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_clock.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 570px,
                    (min-width: 1024px) 570px,
                    (min-width: 768px) 50vw,
                    (min-width: 599px) 50vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="06.4x3_fullbleed-image_clock" />
                        </figure>
                    </div>
                    <div class="cards-holder">
                        <figure class="adaptive-image ">
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
               http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_v2.jpg.rr.640.LOW.jpg 320w,
               http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_v2.jpg.rr.720.LOW.jpg 360w,
               http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_v2.jpg.rr.1198.LOW.jpg 599w,
               http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_v2.jpg.rr.1536.LOW.jpg 768w,
               http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_v2.jpg.rr.1600.LOW.jpg 800w,
               http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_v2.jpg.rr.2048.LOW.jpg 1024w,
               http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_v2.jpg.rr.1160.MED.jpg 1160w,
               http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_v2.jpg.rr.1366.MED.jpg 1366w,
               http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/06.4x3_fullbleed-image_v2.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 570px,
                    (min-width: 1024px) 570px,
                    (min-width: 768px) 50vw,
                    (min-width: 599px) 50vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="06.4x3_fullbleed-image_v2" />
                        </figure>
                    </div>
                </div>
                <div class="spacer">
                </div>
            </div>
        </section>
        <section style="background-color:;" class="rr-section     ">
            <div class="bg">
            </div>
            <div class="children">
                <div class="headline ">
                    <article>
                        <h2 style="color:;  ">The Power of A Bespoke Experience</h2>
                        <div class="introtext h4 ">
                            <p>Our bespoke suits exude simplicity and a powerful statement that speaks volumes. The clean finishes accentuated by the careful stitching eliminates the unnecessary- so as to let the necessary speak for itself.
The finished suit acts as evidence of the prowess of our bespoke team-who work tirelessly to create a garment that creates an air of confidence, leaving a long lasting impression.
</p>
                        </div>
                        <div class="paragraph js-expandable expand">
                            <a href="#" class="expander" data-action="expand"></a>
                            <div class="js-expand expandable">
                                <div>
                                    <p>The finished suit acts as evidence of the prowess of our bespoke team-who work tirelessly to create a garment that creates an air of confidence, leaving a long lasting impression.
                                        <br />
                                        <br /> With its otherworldly presence, Ghost’s purity and restraint shows the skill of the Design team. The interior has the air of confidence and luxury you expect of a <span style="white-space: nowrap;display: inline;padding: 0;">Rolls-Royce.</span> And its understated composure is ever-present, whether you decide to drive or be driven.</p>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <section class="ctacontainer">
                    <div class="inner">
                        <span class="cta   primary">
                            <a href="/experience-bespoke" target="_self" data-action="default" class="h5 ">Explore Further</a>
                        </span>
                    </div>
                </section>
                <figure class="adaptive-image ">
                    <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/05.FAMILY_fullbleed-grid.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/05.FAMILY_fullbleed-grid.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/05.FAMILY_fullbleed-grid.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/05.FAMILY_fullbleed-grid.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/05.FAMILY_fullbleed-grid.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/05.FAMILY_fullbleed-grid.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/05.FAMILY_fullbleed-grid.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/05.FAMILY_fullbleed-grid.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/05.FAMILY_fullbleed-grid.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 100vw,
                    (min-width: 1024px) 100vw,
                    (min-width: 768px) 100vw,
                    (min-width: 599px) 100vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="05.FAMILY_fullbleed-grid" />
                </figure>
            </div>
        </section>
        <section style="background-color:;" class="rr-section     ">
            <div class="bg">
                <figure class="adaptive-image ">
                    <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03-backplate_ink_v1.4_ghost_new.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03-backplate_ink_v1.4_ghost_new.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03-backplate_ink_v1.4_ghost_new.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03-backplate_ink_v1.4_ghost_new.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03-backplate_ink_v1.4_ghost_new.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03-backplate_ink_v1.4_ghost_new.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03-backplate_ink_v1.4_ghost_new.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03-backplate_ink_v1.4_ghost_new.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/03-backplate_ink_v1.4_ghost_new.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 100vw,
                    (min-width: 1024px) 100vw,
                    (min-width: 768px) 100vw,
                    (min-width: 599px) 100vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="03-backplate_ink_v1.4_ghost_new" />
                </figure>
            </div>
            <div class="children">
                <div class="headline ">
                    <article>
                        <h2 class="text-center" style="color:#FFFFFF;  ">Discover Black Badge</h2>
                    </article>
                </div>
                <div class="cards ">
                    <div class="cards-holder single">
                        <article class="js-expandable card">
                            <a href="#" data-action="default">
        <span class="image">
        <figure class="adaptive-image ">
            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== 1w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/08.card_2x1_BB-ghost.jpg.rr.640.LOW.jpg 320w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/08.card_2x1_BB-ghost.jpg.rr.720.LOW.jpg 360w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/08.card_2x1_BB-ghost.jpg.rr.1198.LOW.jpg 599w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/08.card_2x1_BB-ghost.jpg.rr.1536.LOW.jpg 768w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/08.card_2x1_BB-ghost.jpg.rr.1600.LOW.jpg 800w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/08.card_2x1_BB-ghost.jpg.rr.2048.LOW.jpg 1024w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/08.card_2x1_BB-ghost.jpg.rr.1160.MED.jpg 1160w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/08.card_2x1_BB-ghost.jpg.rr.1366.MED.jpg 1366w,
                http://genteel.skyetechgroup.com/wp-content/themes/shopkeeper/bespoke/images/08.card_2x1_BB-ghost.jpg.rr.1920.MED.jpg 1920w" sizes="(min-width: 1200px) 1160px,
                    (min-width: 1024px) 1160px,
                    (min-width: 768px) 100vw,
                    (min-width: 599px) 100vw,
                    (min-width: 320px) 100vw,
                    (min-width: 0px) 100vw,
                    100vw" alt="08.card_2x1_BB-ghost"/>
            
        </figure>
        </span>
        <span class="text" style="display:none;">
            <h3>A darker elegance</h3>
            <p style="font-family: Aleo !important;">Reveal your bolder side with Black Badge. Ghost’s elegant silhouette is unleashed by a darker aesthetic and more potent performance.</p>
        </span>
                    </a>
                        </article>
                    </div>
                </div>
                <div class="spacer">
                </div>
            </div>
        </section>
    </main>
    <script>
    var rrConfig = { "forms": "/bin/rollsroyce/sf/create.json", "configuratorUrl": "configure-12.rolls-roycemotorcars.com/start_intern_nf.html", "dealerSearch": "/services/dealerlocator", "countryList": "/services/dealerlocator/countries", "dealerByCountry": "/services/dealerlocator/country", "siteSearch": "/services/gsa", "googleApiKey": "", "vehicleRecall": "undefined" };
    </script>
    <div class="shade"></div>

<?php get_footer(); ?>

