function stopProgress() {
    progress.find("div").remove(), progressReady = !1;
}

function startProgress() {
    progressReady || progress.append("<div></div>"), progress.find("div").css("width", "0%").transition({
        width: "100%"
    }, screen_time - 100, "linear");
}

function hideScreenMobile(e, t) {
    var n = t ? 1 : -1, i = screens.eq(e);
    i.css("z-index", "1").transition({
        y: -30 * n + "%"
    }, 1500, function() {
        $(this).hide().css("z-index", "1").removeAttr("style").removeClass("first").removeClass("revert").removeClass("regular").find(".screen__visual").removeAttr("style").end().find("h2, .screen__description").removeAttr("style");
    }).find("h2").transition({
        opacity: 0,
        y: -150 * n
    }, 500).end().find(".screen__description").transition({
        y: -60 * n,
        opacity: 0
    }, 400);
}

function showScreenMobile(e, t) {
    var n = t ? 1 : -1, i = t ? "regular" : "revert", r = screens.eq(e);
    screens.not(":eq(" + e + ")").find(".screen__visual").removeClass("zoom"), r.addClass(i).css("z-index", "2").show().transition({
        y: "0%"
    }, 1200).find("h2, h3, p, a").show().css("opacity", "1").end().find("h2").css({
        display: "block",
        opacity: 0
    }).transition({
        y: 0,
        opacity: 1,
        delay: 700
    }, 1500, function() {
        screens_ready = !0, screens.eq(e).find(".screen__visual").addClass("zoom");
    }).end().find(".screen__description").css({
        display: "block",
        y: 20 * n,
        opacity: 0
    }).transition({
        y: 0,
        opacity: 1,
        delay: 1200
    }, 500);
}

function hideScreen(e, t) {
    var n = t ? 1 : -1, i = screens.eq(e);
    i.css("z-index", "1").transition({
        y: -30 * n + "%"
    }, 1500, function() {
        i.hasClass("video1") ? video1.pause() : i.hasClass("video2") && video2.pause(), 
        $(this).hide().css("z-index", "1").removeAttr("style").removeClass("first").removeClass("revert").removeClass("regular").find(".screen__visual").removeAttr("style").end().find("h2, .screen__description, h3, p, a").removeAttr("style").end().find("h2, h3, p, a").hide();
    }).find("h2").transition({
        opacity: 0,
        y: -150 * n
    }, 500);
}

function showScreen(e, t) {
    var n = t ? "regular" : "revert", i = screens.eq(e);
    i.hasClass("video1") ? video1.play() : i.hasClass("video2") && video2.play(), screens.not(":eq(" + e + ")").find(".screen__visual").removeClass("zoom"), 
    i.addClass(n).css("z-index", "2").show().transition({
        y: "0%"
    }, 1200, "easeOutExpo").find(".screen__visual").transition({
        rotate3d: "1,0,0,0deg",
        scale: screen_scale
    }, 1500, function() {
        screens_ready = !0;
    }).end().find("h2").css({
        display: "block",
        opacity: 0
    }).transition({
        y: 0,
        opacity: 1,
        delay: 700
    }, 1500).end().find("h3").css({
        display: "block",
        opacity: 0
    }).transition({
        y: 0,
        opacity: 1,
        delay: 1400
    }, 1600, "easeOutExpo").end().find("p").css({
        display: "block",
        opacity: 0
    }).transition({
        y: 0,
        opacity: 1,
        delay: 1600
    }, 1600, "easeOutExpo", function() {
        screens_ready === !0 && screens.eq(e).find(".screen__visual").addClass("zoom");
    }).end().find("a").css({
        display: "block",
        opacity: 0
    }).transition({
        y: 0,
        opacity: 1,
        delay: 1800
    }, 1600, "easeOutExpo");
}

function setScreen(e, t) {
    var n, i = w.section;
    e ? (n = w.section + 1, n >= screens.length && (n = 0), w.section = n) : (n = w.section - 1, 
    0 > n && (n = screens.length - 1), w.section = n), w.device.mobileJquery ? (hideScreenMobile(i, e), 
    showScreenMobile(n, e)) : (hideScreen(i, e), showScreen(n, e)), t || (clearTimeout(screen_auto_timeout), 
    screen_auto_timeout = setTimeout(startScreens, screen_auto_time));
}

function stopScreens() {
    screens_ready = !1, stopProgress(), clearInterval(screens_interval);
}

function startScreens() {
    over_open || (screens_ready = !0, startProgress(), progressReady = !0, screens_interval = setInterval(function() {
        setScreen(!0, !0), startProgress();
    }, screen_time));
}

function animateOver() {
    over__clients_ready = !0, over.find(".over__clients div").each(function(e, t) {
        $(this).transition({
            y: 0,
            opacity: 1,
            delay: 150 * e
        }, 500);
    });
}

function initPage() {
    $(".screen__item:eq(0)", "#screen").show(), $(".over__contact", ".over").attr("href", "mailto:info@praguebistro.cz"), 
    preloader.css("z-index", "0"), setTimeout(function() {
        preloader.remove();
    }, 1500), $(".homepage_header").transition({
        y: 0,
        opacity: 1,
        delay: 500
    }, 800), startScreens(), w.device.mobileJquery ? showScreenMobile(0, !0) : showScreen(0, !0);
}

function resizeWindow() {
    w.width = $(window).outerWidth(!0), w.height = $(window).outerHeight();
}

!function(e) {
    "function" == typeof define && define.amd && define.amd.jQuery ? define([ "jquery" ], e) : e(jQuery);
}(function(e) {
    function t(t) {
        return !t || void 0 !== t.allowPageScroll || void 0 === t.swipe && void 0 === t.swipeStatus || (t.allowPageScroll = u), 
        void 0 !== t.click && void 0 === t.tap && (t.tap = t.click), t || (t = {}), t = e.extend({}, e.fn.swipe.defaults, t), 
        this.each(function() {
            var i = e(this), r = i.data(E);
            r || (r = new n(this, t), i.data(E, r));
        });
    }
    function n(t, n) {
        function I(t) {
            if (!(ue() || e(t.target).closest(n.excludedElements, He).length > 0)) {
                var i, r = t.originalEvent ? t.originalEvent : t, o = M ? r.touches[0] : r;
                return Re = y, M ? Fe = r.touches.length : t.preventDefault(), qe = 0, Le = null, 
                Ye = null, De = 0, Qe = 0, $e = 0, Be = 1, Ge = 0, Ue = he(), Xe = ve(), ae(), !M || Fe === n.fingers || n.fingers === w || X() ? (de(0, o), 
                We = Te(), 2 == Fe && (de(1, r.touches[1]), Qe = $e = ye(Ue[0].start, Ue[1].start)), 
                (n.swipeStatus || n.pinchStatus) && (i = q(r, Re))) : i = !1, i === !1 ? (Re = x, 
                q(r, Re), i) : (n.hold && (et = setTimeout(e.proxy(function() {
                    He.trigger("hold", [ r.target ]), n.hold && (i = n.hold.call(He, r, r.target));
                }, this), n.longTapThreshold)), le(!0), null);
            }
        }
        function O(e) {
            var t = e.originalEvent ? e.originalEvent : e;
            if (Re !== k && Re !== x && !ce()) {
                var i, r = M ? t.touches[0] : t, o = pe(r);
                if (Je = Te(), M && (Fe = t.touches.length), n.hold && clearTimeout(et), Re = _, 
                2 == Fe && (0 == Qe ? (de(1, t.touches[1]), Qe = $e = ye(Ue[0].start, Ue[1].start)) : (pe(t.touches[1]), 
                $e = ye(Ue[0].end, Ue[1].end), Ye = ke(Ue[0].end, Ue[1].end)), Be = _e(Qe, $e), 
                Ge = Math.abs(Qe - $e)), Fe === n.fingers || n.fingers === w || !M || X()) {
                    if (Le = ze(o.start, o.end), G(e, Le), qe = xe(o.start, o.end), De = be(), me(Le, qe), 
                    (n.swipeStatus || n.pinchStatus) && (i = q(t, Re)), !n.triggerOnTouchEnd || n.triggerOnTouchLeave) {
                        var s = !0;
                        if (n.triggerOnTouchLeave) {
                            var a = Ee(this);
                            s = Ie(o.end, a);
                        }
                        !n.triggerOnTouchEnd && s ? Re = C(_) : n.triggerOnTouchLeave && !s && (Re = C(k)), 
                        (Re == x || Re == k) && q(t, Re);
                    }
                } else Re = x, q(t, Re);
                i === !1 && (Re = x, q(t, Re));
            }
        }
        function S(e) {
            var t = e.originalEvent;
            return M && t.touches.length > 0 ? (se(), !0) : (ce() && (Fe = Ve), Je = Te(), De = be(), 
            Q() || !D() ? (Re = x, q(t, Re)) : n.triggerOnTouchEnd || 0 == n.triggerOnTouchEnd && Re === _ ? (e.preventDefault(), 
            Re = k, q(t, Re)) : !n.triggerOnTouchEnd && N() ? (Re = k, L(t, Re, f)) : Re === _ && (Re = x, 
            q(t, Re)), le(!1), null);
        }
        function j() {
            Fe = 0, Je = 0, We = 0, Qe = 0, $e = 0, Be = 1, ae(), le(!1);
        }
        function P(e) {
            var t = e.originalEvent;
            n.triggerOnTouchLeave && (Re = C(k), q(t, Re));
        }
        function A() {
            He.unbind(Se, I), He.unbind(Ce, j), He.unbind(je, O), He.unbind(Pe, S), Ae && He.unbind(Ae, P), 
            le(!1);
        }
        function C(e) {
            var t = e, i = B(), r = D(), o = Q();
            return !i || o ? t = x : !r || e != _ || n.triggerOnTouchEnd && !n.triggerOnTouchLeave ? !r && e == k && n.triggerOnTouchLeave && (t = x) : t = k, 
            t;
        }
        function q(e, t) {
            var n = void 0;
            return U() || F() ? n = L(e, t, d) : (H() || X()) && n !== !1 && (n = L(e, t, p)), 
            re() && n !== !1 ? n = L(e, t, h) : oe() && n !== !1 ? n = L(e, t, m) : ie() && n !== !1 && (n = L(e, t, f)), 
            t === x && j(e), t === k && (M ? 0 == e.touches.length && j(e) : j(e)), n;
        }
        function L(t, u, l) {
            var g = void 0;
            if (l == d) {
                if (He.trigger("swipeStatus", [ u, Le || null, qe || 0, De || 0, Fe, Ue ]), n.swipeStatus && (g = n.swipeStatus.call(He, t, u, Le || null, qe || 0, De || 0, Fe, Ue), 
                g === !1)) return !1;
                if (u == k && R()) {
                    if (He.trigger("swipe", [ Le, qe, De, Fe, Ue ]), n.swipe && (g = n.swipe.call(He, t, Le, qe, De, Fe, Ue), 
                    g === !1)) return !1;
                    switch (Le) {
                      case i:
                        He.trigger("swipeLeft", [ Le, qe, De, Fe, Ue ]), n.swipeLeft && (g = n.swipeLeft.call(He, t, Le, qe, De, Fe, Ue));
                        break;

                      case r:
                        He.trigger("swipeRight", [ Le, qe, De, Fe, Ue ]), n.swipeRight && (g = n.swipeRight.call(He, t, Le, qe, De, Fe, Ue));
                        break;

                      case o:
                        He.trigger("swipeUp", [ Le, qe, De, Fe, Ue ]), n.swipeUp && (g = n.swipeUp.call(He, t, Le, qe, De, Fe, Ue));
                        break;

                      case s:
                        He.trigger("swipeDown", [ Le, qe, De, Fe, Ue ]), n.swipeDown && (g = n.swipeDown.call(He, t, Le, qe, De, Fe, Ue));
                    }
                }
            }
            if (l == p) {
                if (He.trigger("pinchStatus", [ u, Ye || null, Ge || 0, De || 0, Fe, Be, Ue ]), 
                n.pinchStatus && (g = n.pinchStatus.call(He, t, u, Ye || null, Ge || 0, De || 0, Fe, Be, Ue), 
                g === !1)) return !1;
                if (u == k && Y()) switch (Ye) {
                  case a:
                    He.trigger("pinchIn", [ Ye || null, Ge || 0, De || 0, Fe, Be, Ue ]), n.pinchIn && (g = n.pinchIn.call(He, t, Ye || null, Ge || 0, De || 0, Fe, Be, Ue));
                    break;

                  case c:
                    He.trigger("pinchOut", [ Ye || null, Ge || 0, De || 0, Fe, Be, Ue ]), n.pinchOut && (g = n.pinchOut.call(He, t, Ye || null, Ge || 0, De || 0, Fe, Be, Ue));
                }
            }
            return l == f ? (u === x || u === k) && (clearTimeout(Ke), clearTimeout(et), V() && !ee() ? (Ze = Te(), 
            Ke = setTimeout(e.proxy(function() {
                Ze = null, He.trigger("tap", [ t.target ]), n.tap && (g = n.tap.call(He, t, t.target));
            }, this), n.doubleTapThreshold)) : (Ze = null, He.trigger("tap", [ t.target ]), 
            n.tap && (g = n.tap.call(He, t, t.target)))) : l == h ? (u === x || u === k) && (clearTimeout(Ke), 
            Ze = null, He.trigger("doubletap", [ t.target ]), n.doubleTap && (g = n.doubleTap.call(He, t, t.target))) : l == m && (u === x || u === k) && (clearTimeout(Ke), 
            Ze = null, He.trigger("longtap", [ t.target ]), n.longTap && (g = n.longTap.call(He, t, t.target))), 
            g;
        }
        function D() {
            var e = !0;
            return null !== n.threshold && (e = qe >= n.threshold), e;
        }
        function Q() {
            var e = !1;
            return null !== n.cancelThreshold && null !== Le && (e = ge(Le) - qe >= n.cancelThreshold), 
            e;
        }
        function $() {
            return null === n.pinchThreshold || Ge >= n.pinchThreshold;
        }
        function B() {
            var e;
            return e = !(n.maxTimeThreshold && De >= n.maxTimeThreshold);
        }
        function G(e, t) {
            if (n.allowPageScroll === u || X()) e.preventDefault(); else {
                var a = n.allowPageScroll === l;
                switch (t) {
                  case i:
                    (n.swipeLeft && a || !a && n.allowPageScroll != g) && e.preventDefault();
                    break;

                  case r:
                    (n.swipeRight && a || !a && n.allowPageScroll != g) && e.preventDefault();
                    break;

                  case o:
                    (n.swipeUp && a || !a && n.allowPageScroll != v) && e.preventDefault();
                    break;

                  case s:
                    (n.swipeDown && a || !a && n.allowPageScroll != v) && e.preventDefault();
                }
            }
        }
        function Y() {
            var e = W(), t = J(), n = $();
            return e && t && n;
        }
        function X() {
            return !!(n.pinchStatus || n.pinchIn || n.pinchOut);
        }
        function H() {
            return !(!Y() || !X());
        }
        function R() {
            var e = B(), t = D(), n = W(), i = J(), r = Q(), o = !r && i && n && t && e;
            return o;
        }
        function F() {
            return !!(n.swipe || n.swipeStatus || n.swipeLeft || n.swipeRight || n.swipeUp || n.swipeDown);
        }
        function U() {
            return !(!R() || !F());
        }
        function W() {
            return Fe === n.fingers || n.fingers === w || !M;
        }
        function J() {
            return 0 !== Ue[0].end.x;
        }
        function N() {
            return !!n.tap;
        }
        function V() {
            return !!n.doubleTap;
        }
        function Z() {
            return !!n.longTap;
        }
        function K() {
            if (null == Ze) return !1;
            var e = Te();
            return V() && e - Ze <= n.doubleTapThreshold;
        }
        function ee() {
            return K();
        }
        function te() {
            return (1 === Fe || !M) && (isNaN(qe) || qe < n.threshold);
        }
        function ne() {
            return De > n.longTapThreshold && b > qe;
        }
        function ie() {
            return !(!te() || !N());
        }
        function re() {
            return !(!K() || !V());
        }
        function oe() {
            return !(!ne() || !Z());
        }
        function se() {
            Ne = Te(), Ve = event.touches.length + 1;
        }
        function ae() {
            Ne = 0, Ve = 0;
        }
        function ce() {
            var e = !1;
            if (Ne) {
                var t = Te() - Ne;
                t <= n.fingerReleaseThreshold && (e = !0);
            }
            return e;
        }
        function ue() {
            return !(He.data(E + "_intouch") !== !0);
        }
        function le(e) {
            e === !0 ? (He.bind(je, O), He.bind(Pe, S), Ae && He.bind(Ae, P)) : (He.unbind(je, O, !1), 
            He.unbind(Pe, S, !1), Ae && He.unbind(Ae, P, !1)), He.data(E + "_intouch", e === !0);
        }
        function de(e, t) {
            var n = void 0 !== t.identifier ? t.identifier : 0;
            return Ue[e].identifier = n, Ue[e].start.x = Ue[e].end.x = t.pageX || t.clientX, 
            Ue[e].start.y = Ue[e].end.y = t.pageY || t.clientY, Ue[e];
        }
        function pe(e) {
            var t = void 0 !== e.identifier ? e.identifier : 0, n = fe(t);
            return n.end.x = e.pageX || e.clientX, n.end.y = e.pageY || e.clientY, n;
        }
        function fe(e) {
            for (var t = 0; t < Ue.length; t++) if (Ue[t].identifier == e) return Ue[t];
        }
        function he() {
            for (var e = [], t = 0; 5 >= t; t++) e.push({
                start: {
                    x: 0,
                    y: 0
                },
                end: {
                    x: 0,
                    y: 0
                },
                identifier: 0
            });
            return e;
        }
        function me(e, t) {
            t = Math.max(t, ge(e)), Xe[e].distance = t;
        }
        function ge(e) {
            return Xe[e] ? Xe[e].distance : void 0;
        }
        function ve() {
            var e = {};
            return e[i] = we(i), e[r] = we(r), e[o] = we(o), e[s] = we(s), e;
        }
        function we(e) {
            return {
                direction: e,
                distance: 0
            };
        }
        function be() {
            return Je - We;
        }
        function ye(e, t) {
            var n = Math.abs(e.x - t.x), i = Math.abs(e.y - t.y);
            return Math.round(Math.sqrt(n * n + i * i));
        }
        function _e(e, t) {
            var n = t / e * 1;
            return n.toFixed(2);
        }
        function ke() {
            return 1 > Be ? c : a;
        }
        function xe(e, t) {
            return Math.round(Math.sqrt(Math.pow(t.x - e.x, 2) + Math.pow(t.y - e.y, 2)));
        }
        function Me(e, t) {
            var n = e.x - t.x, i = t.y - e.y, r = Math.atan2(i, n), o = Math.round(180 * r / Math.PI);
            return 0 > o && (o = 360 - Math.abs(o)), o;
        }
        function ze(e, t) {
            var n = Me(e, t);
            return 45 >= n && n >= 0 ? i : 360 >= n && n >= 315 ? i : n >= 135 && 225 >= n ? r : n > 45 && 135 > n ? s : o;
        }
        function Te() {
            var e = new Date();
            return e.getTime();
        }
        function Ee(t) {
            t = e(t);
            var n = t.offset(), i = {
                left: n.left,
                right: n.left + t.outerWidth(),
                top: n.top,
                bottom: n.top + t.outerHeight()
            };
            return i;
        }
        function Ie(e, t) {
            return e.x > t.left && e.x < t.right && e.y > t.top && e.y < t.bottom;
        }
        var Oe = M || T || !n.fallbackToMouseEvents, Se = Oe ? T ? z ? "MSPointerDown" : "pointerdown" : "touchstart" : "mousedown", je = Oe ? T ? z ? "MSPointerMove" : "pointermove" : "touchmove" : "mousemove", Pe = Oe ? T ? z ? "MSPointerUp" : "pointerup" : "touchend" : "mouseup", Ae = Oe ? null : "mouseleave", Ce = T ? z ? "MSPointerCancel" : "pointercancel" : "touchcancel", qe = 0, Le = null, De = 0, Qe = 0, $e = 0, Be = 1, Ge = 0, Ye = 0, Xe = null, He = e(t), Re = "start", Fe = 0, Ue = null, We = 0, Je = 0, Ne = 0, Ve = 0, Ze = 0, Ke = null, et = null;
        try {
            He.bind(Se, I), He.bind(Ce, j);
        } catch (t) {
            e.error("events not supported " + Se + "," + Ce + " on jQuery.swipe");
        }
        this.enable = function() {
            return He.bind(Se, I), He.bind(Ce, j), He;
        }, this.disable = function() {
            return A(), He;
        }, this.destroy = function() {
            return A(), He.data(E, null), He;
        }, this.option = function(t, i) {
            if (void 0 !== n[t]) {
                if (void 0 === i) return n[t];
                n[t] = i;
            } else e.error("Option " + t + " does not exist on jQuery.swipe.options");
            return null;
        };
    }
    var i = "left", r = "right", o = "up", s = "down", a = "in", c = "out", u = "none", l = "auto", d = "swipe", p = "pinch", f = "tap", h = "doubletap", m = "longtap", g = "horizontal", v = "vertical", w = "all", b = 10, y = "start", _ = "move", k = "end", x = "cancel", M = "ontouchstart" in window, z = window.navigator.msPointerEnabled && !window.navigator.pointerEnabled, T = window.navigator.pointerEnabled || window.navigator.msPointerEnabled, E = "TouchSwipe", I = {
        fingers: 1,
        threshold: 75,
        cancelThreshold: null,
        pinchThreshold: 20,
        maxTimeThreshold: null,
        fingerReleaseThreshold: 250,
        longTapThreshold: 500,
        doubleTapThreshold: 200,
        swipe: null,
        swipeLeft: null,
        swipeRight: null,
        swipeUp: null,
        swipeDown: null,
        swipeStatus: null,
        pinchIn: null,
        pinchOut: null,
        pinchStatus: null,
        click: null,
        tap: null,
        doubleTap: null,
        longTap: null,
        hold: null,
        triggerOnTouchEnd: !0,
        triggerOnTouchLeave: !1,
        allowPageScroll: "auto",
        fallbackToMouseEvents: !0,
        excludedElements: "label, button, input, select, textarea, a, .noSwipe"
    };
    e.fn.swipe = function(n) {
        var i = e(this), r = i.data(E);
        if (r && "string" == typeof n) {
            if (r[n]) return r[n].apply(this, Array.prototype.slice.call(arguments, 1));
            e.error("Method " + n + " does not exist on jQuery.swipe");
        } else if (!(r || "object" != typeof n && n)) return t.apply(this, arguments);
        return i;
    }, e.fn.swipe.defaults = I, e.fn.swipe.phases = {
        PHASE_START: y,
        PHASE_MOVE: _,
        PHASE_END: k,
        PHASE_CANCEL: x
    }, e.fn.swipe.directions = {
        LEFT: i,
        RIGHT: r,
        UP: o,
        DOWN: s,
        IN: a,
        OUT: c
    }, e.fn.swipe.pageScroll = {
        NONE: u,
        HORIZONTAL: g,
        VERTICAL: v,
        AUTO: l
    }, e.fn.swipe.fingers = {
        ONE: 1,
        TWO: 2,
        THREE: 3,
        ALL: w
    };
}), function(e) {
    e.html5Loader = function(t) {
        var n = {
            filesToLoad: null,
            debugMode: !1,
            onBeforeLoad: function() {},
            onComplete: function() {},
            onElementLoaded: function(e, t) {},
            onUpdate: function(e) {},
            onMediaError: function(e, t) {}
        }, i = e.extend(n, t), r = i.filesToLoad, o = i.debugMode, s = i.onBeforeLoad, a = i.onComplete, c = i.onElementLoaded, u = i.onUpdate, l = i.onMediaError, d = (e(window), 
        e("body"), 0), p = 0, f = [], h = null != navigator.userAgent.match(/iPad/i), m = function(e) {
            return !(!/android.+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|meego.+mobile|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(e) && !/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(e.substr(0, 4)));
        }(navigator.userAgent || navigator.vendor || window.opera), g = {}, v = function(e) {
            o && console && console.log(e);
        };
        g.video = function() {
            var e = document.createElement("video"), t = !1;
            try {
                (t = !!e.canPlayType) && (t = new Boolean(t), t.ogg = e.canPlayType('video/ogg; codecs="theora"'), 
                t.h264 = e.canPlayType('video/mp4; codecs="avc1.42E01E"'), t.webm = e.canPlayType('video/webm; codecs="vp8, vorbis"'));
            } catch (e) {}
            return t;
        }(), g.audio = function() {
            var e = document.createElement("audio"), t = !1;
            try {
                (t = !!e.canPlayType) && (t = new Boolean(t), t.ogg = e.canPlayType('audio/ogg; codecs="vorbis"'), 
                t.mp3 = e.canPlayType("audio/mpeg;"), t.wav = e.canPlayType('audio/wav; codecs="1"'), 
                t.m4a = e.canPlayType("audio/x-m4a;") || e.canPlayType("audio/aac;"));
            } catch (e) {}
            return t;
        }();
        var w = function(t) {
            var n = t.type.toLowerCase(), i = t.sources;
            return e.each(i, function(e) {
                return g[n][e] ? (t = t.sources[e], t.type = n.toUpperCase(), !1) : void 0;
            }), !!t.source && t;
        }, b = function() {
            var e = 0;
            v("_bytesTotal = " + p), v("_bytesLoaded = " + d), e = Math.round(d / p * 100), 
            v("Percentage: " + e + "%"), u(e), f.length || a();
        }, y = function(e, t) {
            var n = t;
            ("VIDEO" === n.type || "AUDIO" === n.type) && (n = w(n)), n && (p += n.size, f.push(n));
        }, _ = function(t) {
            v("json loaded"), e(t.files).each(y);
        }, k = function(t) {
            var n = new e.Deferred(), i = t.size, r = e("<img>");
            return e(r).on("load", function(e) {
                v("File Loaded:" + t.source), d += i, c(t, r[0]), r = null, f.splice(0, 1), b(), 
                n.resolve();
            }), r.attr("src", t.source), n.promise();
        }, x = function(t) {
            var n = new e.Deferred(), i = t.size, r = e("VIDEO" === t.type ? "<video></video>" : "<audio></audio>"), o = function() {
                v("File Loaded:" + t.source), d += i, c(t, r[0]), f.splice(0, 1), r.off(), r = null, 
                b(), n.resolve();
            };
            return m || h ? o() : (r.on("loadstart", function() {
                3 == this.networkState && (l(t, this), o());
            }), r.on("error stalled", function() {
                l(t, this), o();
            }), r.on("loadedmetadata", function() {
                r.on("progress", function() {
                    var e = 0;
                    v("loading in progress file:" + t.source), this.buffered.length > 0 && (e = i / this.duration * this.buffered.end(0), 
                    i -= e, d += e, b());
                });
            }), r.on("canplaythrough load", o)), r.attr({
                preload: "auto",
                src: t.source,
                controls: "controls"
            }), n.promise();
        }, M = function(t) {
            var n = new e.Deferred(), i = t.size;
            return e.getScript(t.source, function(e) {
                v("File Loaded:" + t.source), d += i, c(t, e), f.splice(0, 1), b(), n.resolve();
            }), n.promise();
        }, z = function(t) {
            var n = new e.Deferred();
            return e.ajax({
                url: t.source,
                dataType: "text",
                success: function(e) {
                    v("File Loaded:" + t.source), c(t, e), d += t.size, f.splice(0, 1), b(), n.resolve(e);
                }
            }), n.promise();
        }, T = function() {
            var t = f.slice();
            e.each(t, function(e, t) {
                switch (v("preloading files"), v("file to preload:" + t.source), t.type) {
                  case "IMAGE":
                    k(t);
                    break;

                  case "VIDEO":
                  case "AUDIO":
                    x(t);
                    break;

                  case "SCRIPT":
                    M(t);
                    break;

                  case "TEXT":
                    z(t);
                    break;

                  default:
                    return !1;
                }
            });
        };
        return this.init = function() {
            v("plugin initialized");
            var t = new e.Deferred(), n = t.promise();
            s(), "object" == typeof r ? (e.proxy(_, this, r)(), t.resolve(r)) : (e.getJSON(r, t.resolve), 
            t.pipe(e.proxy(_, this))), n.then(e.proxy(b, this)), n.then(e.proxy(T, this));
        }, this.init(), this;
    };
}(jQuery), function(e, t) {
    "function" == typeof define && define.amd ? define([ "jquery" ], t) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(e.jQuery);
}(this, function(e) {
    function t(e) {
        if (e in d.style) return e;
        for (var t = [ "Moz", "Webkit", "O", "ms" ], n = e.charAt(0).toUpperCase() + e.substr(1), i = 0; i < t.length; ++i) {
            var r = t[i] + n;
            if (r in d.style) return r;
        }
    }
    function n() {
        return d.style[p.transform] = "", d.style[p.transform] = "rotateY(90deg)", "" !== d.style[p.transform];
    }
    function i(e) {
        return "string" == typeof e && this.parse(e), this;
    }
    function r(e, t, n) {
        t === !0 ? e.queue(n) : t ? e.queue(t, n) : e.each(function() {
            n.call(this);
        });
    }
    function o(t) {
        var n = [];
        return e.each(t, function(t) {
            t = e.camelCase(t), t = e.transit.propertyMap[t] || e.cssProps[t] || t, t = c(t), 
            p[t] && (t = c(p[t])), -1 === e.inArray(t, n) && n.push(t);
        }), n;
    }
    function s(t, n, i, r) {
        var s = o(t);
        e.cssEase[i] && (i = e.cssEase[i]);
        var a = "" + l(n) + " " + i;
        parseInt(r, 10) > 0 && (a += " " + l(r));
        var c = [];
        return e.each(s, function(e, t) {
            c.push(t + " " + a);
        }), c.join(", ");
    }
    function a(t, n) {
        n || (e.cssNumber[t] = !0), e.transit.propertyMap[t] = p.transform, e.cssHooks[t] = {
            get: function(n) {
                var i = e(n).css("transit:transform");
                return i.get(t);
            },
            set: function(n, i) {
                var r = e(n).css("transit:transform");
                r.setFromString(t, i), e(n).css({
                    "transit:transform": r
                });
            }
        };
    }
    function c(e) {
        return e.replace(/([A-Z])/g, function(e) {
            return "-" + e.toLowerCase();
        });
    }
    function u(e, t) {
        return "string" != typeof e || e.match(/^[\-0-9\.]+$/) ? "" + e + t : e;
    }
    function l(t) {
        var n = t;
        return "string" != typeof n || n.match(/^[\-0-9\.]+/) || (n = e.fx.speeds[n] || e.fx.speeds._default), 
        u(n, "ms");
    }
    e.transit = {
        version: "0.9.12",
        propertyMap: {
            marginLeft: "margin",
            marginRight: "margin",
            marginBottom: "margin",
            marginTop: "margin",
            paddingLeft: "padding",
            paddingRight: "padding",
            paddingBottom: "padding",
            paddingTop: "padding"
        },
        enabled: !0,
        useTransitionEnd: !1
    };
    var d = document.createElement("div"), p = {}, f = navigator.userAgent.toLowerCase().indexOf("chrome") > -1;
    p.transition = t("transition"), p.transitionDelay = t("transitionDelay"), p.transform = t("transform"), 
    p.transformOrigin = t("transformOrigin"), p.filter = t("Filter"), p.transform3d = n();
    var h = {
        transition: "transitionend",
        MozTransition: "transitionend",
        OTransition: "oTransitionEnd",
        WebkitTransition: "webkitTransitionEnd",
        msTransition: "MSTransitionEnd"
    }, m = p.transitionEnd = h[p.transition] || null;
    for (var g in p) p.hasOwnProperty(g) && "undefined" == typeof e.support[g] && (e.support[g] = p[g]);
    return d = null, e.cssEase = {
        _default: "ease",
        in: "ease-in",
        out: "ease-out",
        "in-out": "ease-in-out",
        snap: "cubic-bezier(0,1,.5,1)",
        easeInCubic: "cubic-bezier(.550,.055,.675,.190)",
        easeOutCubic: "cubic-bezier(.215,.61,.355,1)",
        easeInOutCubic: "cubic-bezier(.645,.045,.355,1)",
        easeInCirc: "cubic-bezier(.6,.04,.98,.335)",
        easeOutCirc: "cubic-bezier(.075,.82,.165,1)",
        easeInOutCirc: "cubic-bezier(.785,.135,.15,.86)",
        easeInExpo: "cubic-bezier(.95,.05,.795,.035)",
        easeOutExpo: "cubic-bezier(.19,1,.22,1)",
        easeInOutExpo: "cubic-bezier(1,0,0,1)",
        easeInQuad: "cubic-bezier(.55,.085,.68,.53)",
        easeOutQuad: "cubic-bezier(.25,.46,.45,.94)",
        easeInOutQuad: "cubic-bezier(.455,.03,.515,.955)",
        easeInQuart: "cubic-bezier(.895,.03,.685,.22)",
        easeOutQuart: "cubic-bezier(.165,.84,.44,1)",
        easeInOutQuart: "cubic-bezier(.77,0,.175,1)",
        easeInQuint: "cubic-bezier(.755,.05,.855,.06)",
        easeOutQuint: "cubic-bezier(.23,1,.32,1)",
        easeInOutQuint: "cubic-bezier(.86,0,.07,1)",
        easeInSine: "cubic-bezier(.47,0,.745,.715)",
        easeOutSine: "cubic-bezier(.39,.575,.565,1)",
        easeInOutSine: "cubic-bezier(.445,.05,.55,.95)",
        easeInBack: "cubic-bezier(.6,-.28,.735,.045)",
        easeOutBack: "cubic-bezier(.175, .885,.32,1.275)",
        easeInOutBack: "cubic-bezier(.68,-.55,.265,1.55)"
    }, e.cssHooks["transit:transform"] = {
        get: function(t) {
            return e(t).data("transform") || new i();
        },
        set: function(t, n) {
            var r = n;
            r instanceof i || (r = new i(r)), t.style[p.transform] = "WebkitTransform" !== p.transform || f ? r.toString() : r.toString(!0), 
            e(t).data("transform", r);
        }
    }, e.cssHooks.transform = {
        set: e.cssHooks["transit:transform"].set
    }, e.cssHooks.filter = {
        get: function(e) {
            return e.style[p.filter];
        },
        set: function(e, t) {
            e.style[p.filter] = t;
        }
    }, e.fn.jquery < "1.8" && (e.cssHooks.transformOrigin = {
        get: function(e) {
            return e.style[p.transformOrigin];
        },
        set: function(e, t) {
            e.style[p.transformOrigin] = t;
        }
    }, e.cssHooks.transition = {
        get: function(e) {
            return e.style[p.transition];
        },
        set: function(e, t) {
            e.style[p.transition] = t;
        }
    }), a("scale"), a("scaleX"), a("scaleY"), a("translate"), a("rotate"), a("rotateX"), 
    a("rotateY"), a("rotate3d"), a("perspective"), a("skewX"), a("skewY"), a("x", !0), 
    a("y", !0), i.prototype = {
        setFromString: function(e, t) {
            var n = "string" == typeof t ? t.split(",") : t.constructor === Array ? t : [ t ];
            n.unshift(e), i.prototype.set.apply(this, n);
        },
        set: function(e) {
            var t = Array.prototype.slice.apply(arguments, [ 1 ]);
            this.setter[e] ? this.setter[e].apply(this, t) : this[e] = t.join(",");
        },
        get: function(e) {
            return this.getter[e] ? this.getter[e].apply(this) : this[e] || 0;
        },
        setter: {
            rotate: function(e) {
                this.rotate = u(e, "deg");
            },
            rotateX: function(e) {
                this.rotateX = u(e, "deg");
            },
            rotateY: function(e) {
                this.rotateY = u(e, "deg");
            },
            scale: function(e, t) {
                void 0 === t && (t = e), this.scale = e + "," + t;
            },
            skewX: function(e) {
                this.skewX = u(e, "deg");
            },
            skewY: function(e) {
                this.skewY = u(e, "deg");
            },
            perspective: function(e) {
                this.perspective = u(e, "px");
            },
            x: function(e) {
                this.set("translate", e, null);
            },
            y: function(e) {
                this.set("translate", null, e);
            },
            translate: function(e, t) {
                void 0 === this._translateX && (this._translateX = 0), void 0 === this._translateY && (this._translateY = 0), 
                null !== e && void 0 !== e && (this._translateX = u(e, "px")), null !== t && void 0 !== t && (this._translateY = u(t, "px")), 
                this.translate = this._translateX + "," + this._translateY;
            }
        },
        getter: {
            x: function() {
                return this._translateX || 0;
            },
            y: function() {
                return this._translateY || 0;
            },
            scale: function() {
                var e = (this.scale || "1,1").split(",");
                return e[0] && (e[0] = parseFloat(e[0])), e[1] && (e[1] = parseFloat(e[1])), e[0] === e[1] ? e[0] : e;
            },
            rotate3d: function() {
                for (var e = (this.rotate3d || "0,0,0,0deg").split(","), t = 0; 3 >= t; ++t) e[t] && (e[t] = parseFloat(e[t]));
                return e[3] && (e[3] = u(e[3], "deg")), e;
            }
        },
        parse: function(e) {
            var t = this;
            e.replace(/([a-zA-Z0-9]+)\((.*?)\)/g, function(e, n, i) {
                t.setFromString(n, i);
            });
        },
        toString: function(e) {
            var t = [];
            for (var n in this) if (this.hasOwnProperty(n)) {
                if (!p.transform3d && ("rotateX" === n || "rotateY" === n || "perspective" === n || "transformOrigin" === n)) continue;
                "_" !== n[0] && t.push(e && "scale" === n ? n + "3d(" + this[n] + ",1)" : e && "translate" === n ? n + "3d(" + this[n] + ",0)" : n + "(" + this[n] + ")");
            }
            return t.join(" ");
        }
    }, e.fn.transition = e.fn.transit = function(t, n, i, o) {
        var a = this, c = 0, u = !0, d = e.extend(!0, {}, t);
        "function" == typeof n && (o = n, n = void 0), "object" == typeof n && (i = n.easing, 
        c = n.delay || 0, u = "undefined" == typeof n.queue || n.queue, o = n.complete, 
        n = n.duration), "function" == typeof i && (o = i, i = void 0), "undefined" != typeof d.easing && (i = d.easing, 
        delete d.easing), "undefined" != typeof d.duration && (n = d.duration, delete d.duration), 
        "undefined" != typeof d.complete && (o = d.complete, delete d.complete), "undefined" != typeof d.queue && (u = d.queue, 
        delete d.queue), "undefined" != typeof d.delay && (c = d.delay, delete d.delay), 
        "undefined" == typeof n && (n = e.fx.speeds._default), "undefined" == typeof i && (i = e.cssEase._default), 
        n = l(n);
        var f = s(d, n, i, c), h = e.transit.enabled && p.transition, g = h ? parseInt(n, 10) + parseInt(c, 10) : 0;
        if (0 === g) {
            var v = function(e) {
                a.css(d), o && o.apply(a), e && e();
            };
            return r(a, u, v), a;
        }
        var w = {}, b = function(t) {
            var n = !1, i = function() {
                n && a.unbind(m, i), g > 0 && a.each(function() {
                    this.style[p.transition] = w[this] || null;
                }), "function" == typeof o && o.apply(a), "function" == typeof t && t();
            };
            g > 0 && m && e.transit.useTransitionEnd ? (n = !0, a.bind(m, i)) : window.setTimeout(i, g), 
            a.each(function() {
                g > 0 && (this.style[p.transition] = f), e(this).css(d);
            });
        }, y = function(e) {
            this.offsetWidth, b(e);
        };
        return r(a, u, y), this;
    }, e.transit.getTransitionValue = s, e;
}), !function(e, t) {
    "undefined" != typeof module && module.exports ? module.exports.browser = t() : "function" == typeof define && define.amd ? define(t) : this[e] = t();
}("bowser", function() {
    function e(e) {
        function n(t) {
            var n = e.match(t);
            return n && n.length > 1 && n[1] || "";
        }
        var i, r = n(/(ipod|iphone|ipad)/i).toLowerCase(), o = /like android/i.test(e), s = !o && /android/i.test(e), a = n(/version\/(\d+(\.\d+)?)/i), c = /tablet/i.test(e), u = !c && /[^-]mobi/i.test(e);
        /opera|opr/i.test(e) ? i = {
            name: "Opera",
            opera: t,
            version: a || n(/(?:opera|opr)[\s\/](\d+(\.\d+)?)/i)
        } : /windows phone/i.test(e) ? i = {
            name: "Windows Phone",
            windowsphone: t,
            msie: t,
            version: n(/iemobile\/(\d+(\.\d+)?)/i)
        } : /msie|trident/i.test(e) ? i = {
            name: "Internet Explorer",
            msie: t,
            version: n(/(?:msie |rv:)(\d+(\.\d+)?)/i)
        } : /chrome|crios|crmo/i.test(e) ? i = {
            name: "Chrome",
            chrome: t,
            version: n(/(?:chrome|crios|crmo)\/(\d+(\.\d+)?)/i)
        } : r ? (i = {
            name: "iphone" == r ? "iPhone" : "ipad" == r ? "iPad" : "iPod"
        }, a && (i.version = a)) : /sailfish/i.test(e) ? i = {
            name: "Sailfish",
            sailfish: t,
            version: n(/sailfish\s?browser\/(\d+(\.\d+)?)/i)
        } : /seamonkey\//i.test(e) ? i = {
            name: "SeaMonkey",
            seamonkey: t,
            version: n(/seamonkey\/(\d+(\.\d+)?)/i)
        } : /firefox|iceweasel/i.test(e) ? (i = {
            name: "Firefox",
            firefox: t,
            version: n(/(?:firefox|iceweasel)[ \/](\d+(\.\d+)?)/i)
        }, /\((mobile|tablet);[^\)]*rv:[\d\.]+\)/i.test(e) && (i.firefoxos = t)) : /silk/i.test(e) ? i = {
            name: "Amazon Silk",
            silk: t,
            version: n(/silk\/(\d+(\.\d+)?)/i)
        } : s ? i = {
            name: "Android",
            version: a
        } : /phantom/i.test(e) ? i = {
            name: "PhantomJS",
            phantom: t,
            version: n(/phantomjs\/(\d+(\.\d+)?)/i)
        } : /blackberry|\bbb\d+/i.test(e) || /rim\stablet/i.test(e) ? i = {
            name: "BlackBerry",
            blackberry: t,
            version: a || n(/blackberry[\d]+\/(\d+(\.\d+)?)/i)
        } : /(web|hpw)os/i.test(e) ? (i = {
            name: "WebOS",
            webos: t,
            version: a || n(/w(?:eb)?osbrowser\/(\d+(\.\d+)?)/i)
        }, /touchpad\//i.test(e) && (i.touchpad = t)) : i = /bada/i.test(e) ? {
            name: "Bada",
            bada: t,
            version: n(/dolfin\/(\d+(\.\d+)?)/i)
        } : /tizen/i.test(e) ? {
            name: "Tizen",
            tizen: t,
            version: n(/(?:tizen\s?)?browser\/(\d+(\.\d+)?)/i) || a
        } : /safari/i.test(e) ? {
            name: "Safari",
            safari: t,
            version: a
        } : {}, /(apple)?webkit/i.test(e) ? (i.name = i.name || "Webkit", i.webkit = t, 
        !i.version && a && (i.version = a)) : !i.opera && /gecko\//i.test(e) && (i.name = i.name || "Gecko", 
        i.gecko = t, i.version = i.version || n(/gecko\/(\d+(\.\d+)?)/i)), s || i.silk ? i.android = t : r && (i[r] = t, 
        i.ios = t);
        var l = "";
        r ? (l = n(/os (\d+([_\s]\d+)*) like mac os x/i), l = l.replace(/[_\s]/g, ".")) : s ? l = n(/android[ \/-](\d+(\.\d+)*)/i) : i.windowsphone ? l = n(/windows phone (?:os)?\s?(\d+(\.\d+)*)/i) : i.webos ? l = n(/(?:web|hpw)os\/(\d+(\.\d+)*)/i) : i.blackberry ? l = n(/rim\stablet\sos\s(\d+(\.\d+)*)/i) : i.bada ? l = n(/bada\/(\d+(\.\d+)*)/i) : i.tizen && (l = n(/tizen[\/\s](\d+(\.\d+)*)/i)), 
        l && (i.osversion = l);
        var d = l.split(".")[0];
        return c || "ipad" == r || s && (3 == d || 4 == d && !u) || i.silk ? i.tablet = t : (u || "iphone" == r || "ipod" == r || s || i.blackberry || i.webos || i.bada) && (i.mobile = t), 
        i.msie && i.version >= 10 || i.chrome && i.version >= 20 || i.firefox && i.version >= 20 || i.safari && i.version >= 6 || i.opera && i.version >= 10 || i.ios && i.osversion && i.osversion.split(".")[0] >= 6 || i.blackberry && i.version >= 10.1 ? i.a = t : i.msie && i.version < 10 || i.chrome && i.version < 20 || i.firefox && i.version < 20 || i.safari && i.version < 6 || i.opera && i.version < 10 || i.ios && i.osversion && i.osversion.split(".")[0] < 6 ? i.c = t : i.x = t, 
        i;
    }
    var t = !0, n = e("undefined" != typeof navigator ? navigator.userAgent : "");
    return n._detect = e, n;
}), !function(e) {
    "function" == typeof define && define.amd ? define([ "jquery" ], e) : "object" == typeof exports ? module.exports = e : e(jQuery);
}(function(e) {
    function t(t) {
        var s = t || window.event, a = c.call(arguments, 1), u = 0, d = 0, p = 0, f = 0, h = 0, m = 0;
        if (t = e.event.fix(s), t.type = "mousewheel", "detail" in s && (p = -1 * s.detail), 
        "wheelDelta" in s && (p = s.wheelDelta), "wheelDeltaY" in s && (p = s.wheelDeltaY), 
        "wheelDeltaX" in s && (d = -1 * s.wheelDeltaX), "axis" in s && s.axis === s.HORIZONTAL_AXIS && (d = -1 * p, 
        p = 0), u = 0 === p ? d : p, "deltaY" in s && (p = -1 * s.deltaY, u = p), "deltaX" in s && (d = s.deltaX, 
        0 === p && (u = -1 * d)), 0 !== p || 0 !== d) {
            if (1 === s.deltaMode) {
                var g = e.data(this, "mousewheel-line-height");
                u *= g, p *= g, d *= g;
            } else if (2 === s.deltaMode) {
                var v = e.data(this, "mousewheel-page-height");
                u *= v, p *= v, d *= v;
            }
            if (f = Math.max(Math.abs(p), Math.abs(d)), (!o || o > f) && (o = f, i(s, f) && (o /= 40)), 
            i(s, f) && (u /= 40, d /= 40, p /= 40), u = Math[u >= 1 ? "floor" : "ceil"](u / o), 
            d = Math[d >= 1 ? "floor" : "ceil"](d / o), p = Math[p >= 1 ? "floor" : "ceil"](p / o), 
            l.settings.normalizeOffset && this.getBoundingClientRect) {
                var w = this.getBoundingClientRect();
                h = t.clientX - w.left, m = t.clientY - w.top;
            }
            return t.deltaX = d, t.deltaY = p, t.deltaFactor = o, t.offsetX = h, t.offsetY = m, 
            t.deltaMode = 0, a.unshift(t, u, d, p), r && clearTimeout(r), r = setTimeout(n, 200), 
            (e.event.dispatch || e.event.handle).apply(this, a);
        }
    }
    function n() {
        o = null;
    }
    function i(e, t) {
        return l.settings.adjustOldDeltas && "mousewheel" === e.type && t % 120 === 0;
    }
    var r, o, s = [ "wheel", "mousewheel", "DOMMouseScroll", "MozMousePixelScroll" ], a = "onwheel" in document || document.documentMode >= 9 ? [ "wheel" ] : [ "mousewheel", "DomMouseScroll", "MozMousePixelScroll" ], c = Array.prototype.slice;
    if (e.event.fixHooks) for (var u = s.length; u; ) e.event.fixHooks[s[--u]] = e.event.mouseHooks;
    var l = e.event.special.mousewheel = {
        version: "3.1.12",
        setup: function() {
            if (this.addEventListener) for (var n = a.length; n; ) this.addEventListener(a[--n], t, !1); else this.onmousewheel = t;
            e.data(this, "mousewheel-line-height", l.getLineHeight(this)), e.data(this, "mousewheel-page-height", l.getPageHeight(this));
        },
        teardown: function() {
            if (this.removeEventListener) for (var n = a.length; n; ) this.removeEventListener(a[--n], t, !1); else this.onmousewheel = null;
            e.removeData(this, "mousewheel-line-height"), e.removeData(this, "mousewheel-page-height");
        },
        getLineHeight: function(t) {
            var n = e(t), i = n["offsetParent" in e.fn ? "offsetParent" : "parent"]();
            return i.length || (i = e("body")), parseInt(i.css("fontSize"), 10) || parseInt(n.css("fontSize"), 10) || 16;
        },
        getPageHeight: function(t) {
            return e(t).height();
        },
        settings: {
            adjustOldDeltas: !0,
            normalizeOffset: !0
        }
    };
    e.fn.extend({
        mousewheel: function(e) {
            return e ? this.bind("mousewheel", e) : this.trigger("mousewheel");
        },
        unmousewheel: function(e) {
            return this.unbind("mousewheel", e);
        }
    });
}), function() {
    for (var e, t = function() {}, n = [ "assert", "clear", "count", "debug", "dir", "dirxml", "error", "exception", "group", "groupCollapsed", "groupEnd", "info", "log", "markTimeline", "profile", "profileEnd", "table", "time", "timeEnd", "timeStamp", "trace", "warn" ], i = n.length, r = window.console = window.console || {}; i--; ) e = n[i], 
    r[e] || (r[e] = t);
}(), function(e) {
    (jQuery.browser = jQuery.browser || {}).mobile = /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(e) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(e.substr(0, 4));
}(navigator.userAgent || navigator.vendor || window.opera), jQuery.easing.jswing = jQuery.easing.swing, 
jQuery.extend(jQuery.easing, {
    def: "easeOutQuad",
    swing: function(e, t, n, i, r) {
        return jQuery.easing[jQuery.easing.def](e, t, n, i, r);
    },
    easeInQuad: function(e, t, n, i, r) {
        return i * (t /= r) * t + n;
    },
    easeOutQuad: function(e, t, n, i, r) {
        return -i * (t /= r) * (t - 2) + n;
    },
    easeInOutQuad: function(e, t, n, i, r) {
        return (t /= r / 2) < 1 ? i / 2 * t * t + n : -i / 2 * (--t * (t - 2) - 1) + n;
    },
    easeInCubic: function(e, t, n, i, r) {
        return i * (t /= r) * t * t + n;
    },
    easeOutCubic: function(e, t, n, i, r) {
        return i * ((t = t / r - 1) * t * t + 1) + n;
    },
    easeInOutCubic: function(e, t, n, i, r) {
        return (t /= r / 2) < 1 ? i / 2 * t * t * t + n : i / 2 * ((t -= 2) * t * t + 2) + n;
    },
    easeInQuart: function(e, t, n, i, r) {
        return i * (t /= r) * t * t * t + n;
    },
    easeOutQuart: function(e, t, n, i, r) {
        return -i * ((t = t / r - 1) * t * t * t - 1) + n;
    },
    easeInOutQuart: function(e, t, n, i, r) {
        return (t /= r / 2) < 1 ? i / 2 * t * t * t * t + n : -i / 2 * ((t -= 2) * t * t * t - 2) + n;
    },
    easeInQuint: function(e, t, n, i, r) {
        return i * (t /= r) * t * t * t * t + n;
    },
    easeOutQuint: function(e, t, n, i, r) {
        return i * ((t = t / r - 1) * t * t * t * t + 1) + n;
    },
    easeInOutQuint: function(e, t, n, i, r) {
        return (t /= r / 2) < 1 ? i / 2 * t * t * t * t * t + n : i / 2 * ((t -= 2) * t * t * t * t + 2) + n;
    },
    easeInSine: function(e, t, n, i, r) {
        return -i * Math.cos(t / r * (Math.PI / 2)) + i + n;
    },
    easeOutSine: function(e, t, n, i, r) {
        return i * Math.sin(t / r * (Math.PI / 2)) + n;
    },
    easeInOutSine: function(e, t, n, i, r) {
        return -i / 2 * (Math.cos(Math.PI * t / r) - 1) + n;
    },
    easeInExpo: function(e, t, n, i, r) {
        return 0 == t ? n : i * Math.pow(2, 10 * (t / r - 1)) + n;
    },
    easeOutExpo: function(e, t, n, i, r) {
        return t == r ? n + i : i * (-Math.pow(2, -10 * t / r) + 1) + n;
    },
    easeInOutExpo: function(e, t, n, i, r) {
        return 0 == t ? n : t == r ? n + i : (t /= r / 2) < 1 ? i / 2 * Math.pow(2, 10 * (t - 1)) + n : i / 2 * (-Math.pow(2, -10 * --t) + 2) + n;
    },
    easeInCirc: function(e, t, n, i, r) {
        return -i * (Math.sqrt(1 - (t /= r) * t) - 1) + n;
    },
    easeOutCirc: function(e, t, n, i, r) {
        return i * Math.sqrt(1 - (t = t / r - 1) * t) + n;
    },
    easeInOutCirc: function(e, t, n, i, r) {
        return (t /= r / 2) < 1 ? -i / 2 * (Math.sqrt(1 - t * t) - 1) + n : i / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + n;
    },
    easeInElastic: function(e, t, n, i, r) {
        var o = 1.70158, s = 0, a = i;
        if (0 == t) return n;
        if (1 == (t /= r)) return n + i;
        if (s || (s = .3 * r), a < Math.abs(i)) {
            a = i;
            var o = s / 4;
        } else var o = s / (2 * Math.PI) * Math.asin(i / a);
        return -(a * Math.pow(2, 10 * (t -= 1)) * Math.sin(2 * (t * r - o) * Math.PI / s)) + n;
    },
    easeOutElastic: function(e, t, n, i, r) {
        var o = 1.70158, s = 0, a = i;
        if (0 == t) return n;
        if (1 == (t /= r)) return n + i;
        if (s || (s = .3 * r), a < Math.abs(i)) {
            a = i;
            var o = s / 4;
        } else var o = s / (2 * Math.PI) * Math.asin(i / a);
        return a * Math.pow(2, -10 * t) * Math.sin(2 * (t * r - o) * Math.PI / s) + i + n;
    },
    easeInOutElastic: function(e, t, n, i, r) {
        var o = 1.70158, s = 0, a = i;
        if (0 == t) return n;
        if (2 == (t /= r / 2)) return n + i;
        if (s || (s = .3 * r * 1.5), a < Math.abs(i)) {
            a = i;
            var o = s / 4;
        } else var o = s / (2 * Math.PI) * Math.asin(i / a);
        return 1 > t ? -.5 * a * Math.pow(2, 10 * (t -= 1)) * Math.sin(2 * (t * r - o) * Math.PI / s) + n : a * Math.pow(2, -10 * (t -= 1)) * Math.sin(2 * (t * r - o) * Math.PI / s) * .5 + i + n;
    },
    easeInBack: function(e, t, n, i, r, o) {
        return void 0 == o && (o = 1.70158), i * (t /= r) * t * ((o + 1) * t - o) + n;
    },
    easeOutBack: function(e, t, n, i, r, o) {
        return void 0 == o && (o = 1.70158), i * ((t = t / r - 1) * t * ((o + 1) * t + o) + 1) + n;
    },
    easeInOutBack: function(e, t, n, i, r, o) {
        return void 0 == o && (o = 1.70158), (t /= r / 2) < 1 ? i / 2 * t * t * (((o *= 1.525) + 1) * t - o) + n : i / 2 * ((t -= 2) * t * (((o *= 1.525) + 1) * t + o) + 2) + n;
    },
    easeInBounce: function(e, t, n, i, r) {
        return i - jQuery.easing.easeOutBounce(e, r - t, 0, i, r) + n;
    },
    easeOutBounce: function(e, t, n, i, r) {
        return (t /= r) < 1 / 2.75 ? 7.5625 * i * t * t + n : 2 / 2.75 > t ? i * (7.5625 * (t -= 1.5 / 2.75) * t + .75) + n : 2.5 / 2.75 > t ? i * (7.5625 * (t -= 2.25 / 2.75) * t + .9375) + n : i * (7.5625 * (t -= 2.625 / 2.75) * t + .984375) + n;
    },
    easeInOutBounce: function(e, t, n, i, r) {
        return r / 2 > t ? .5 * jQuery.easing.easeInBounce(e, 2 * t, 0, i, r) + n : .5 * jQuery.easing.easeOutBounce(e, 2 * t - r, 0, i, r) + .5 * i + n;
    }
});

var w = {
    width: $(window).outerWidth(!0),
    height: $(window).outerHeight(),
    top: $(window).scrollTop(),
    resolution: {
        desktop: !1,
        tablet: !1,
        mobile: !1
    },
    device: {
        mobileBowser: bowser.mobile,
        mobileJquery: $("html").hasClass("mobileDevice"),
        tablet: bowser.tablet,
        ios: bowser.ios,
        os: null,
        osversion: bowser.osversion
    },
    browser: {
        name: null,
        v: parseInt(bowser.version),
        webkit: bowser.webkit,
        gecko: bowser.gecko
    },
    section: 0
};

bowser.chrome && (w.browser.name = "chrome"), bowser.safari && (w.browser.name = "safari"), 
bowser.opera && (w.browser.name = "opera"), bowser.mozzila && (w.browser.name = "mozzila"), 
bowser.firefox && (w.browser.name = "firefox"), bowser.msie && (w.browser.name = "msie"), 
w.device.mobile ? (bowser.android && (w.device.os = "android"), bowser.ios && (w.device.os = "ios"), 
bowser.blackberry && (w.device.os = "blackberry"), bowser.firefoxos && (w.device.os = "firefoxos"), 
$("html").addClass("os_" + w.device.os), $("html").addClass("mobileDevice")) : ($("html").addClass(w.browser.name), 
$("html").addClass("v" + w.browser.v)), w.device.tablet && $("html").addClass("tablet"), 
w.device.mobileJquery && $("html").addClass("mobile"), w.device.mobileJquery || w.device.tablet || $("html").addClass("desktop"), 
$.support.transition || ($.fn.transition = $.fn.animate);

var preloader = $("#preloader"), preloaderBar = preloader.find(".bar"), dir_video = "web-public/video/", dir_images = (!w.device.mobileJquery && w.width > 1250, 
"images/"), preloaderFiles, logo = $("#logo"), over_open = !1, over__clients_top = 982, over__clients_ready = !1, screens = $("#screen").find((w.device.mobileJquery, 
".screen__item")), screens_ready = !0, screens_interval, screen_time = 7e3, screen_scale = 1.06;

(bowser.firefox || bowser.mozzila) && (screen_scale = 1);

var screen_auto_timeout, screen_auto_time = 1e4, progress = $("#progress"), progressReady = !1, video1 = document.getElementById("video1"), video2 = document.getElementById("video2");

if (preloaderFiles = {
    files: [ {
        type: "IMAGE",
        source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/genteel-home.png",
        size: 3
    }, {
        type: "IMAGE",
        source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/genteel_home_logo_m.png",
        size: 2
    }, {
        type: "IMAGE",
        source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/4.jpg",
        size: 521
    }, {
        type: "IMAGE",
        source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/1.jpg",
        size: 521
    }, {
        type: "IMAGE",
        source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/2.jpg",
        size: 521
    }, {
        type: "IMAGE",
        source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/4.jpg",
        size: 521
    }, {
        type: "IMAGE",
        source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/5.jpg",
        size: 521
    }, {
        type: "IMAGE",
        source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/6.jpg",
        size: 521
    }, {
        type: "IMAGE",
        source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/7.jpg",
        size: 521
    }, {
        type: "IMAGE",
        source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/4.jpg",
        size: 521
    }, {
        type: "IMAGE",
        source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/loeries2016.jpg",
        size: 521
    }, {
        type: "IMAGE",
        source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/loeries.jpg",
        size: 521
    }, {
        type: "IMAGE",
        source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/2.jpg",
        size: 521
    } ]
}, !(w.device.mobileJquery || bowser.mozilla || bowser.firefox || bowser.safari)) {
    var video_quality = "480";
    preloaderFiles = {
        files: [ {
            type: "IMAGE",
            source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/genteel-home.png",
            size: 3
        }, {
            type: "IMAGE",
            source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/genteel_home_logo_m.png",
            size: 2
        }, {
            type: "IMAGE",
            source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/4.jpg",
            size: 521
        }, {
            type: "IMAGE",
            source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/1.jpg",
            size: 521
        }, {
            type: "IMAGE",
            source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/2.jpg",
            size: 521
        }, {
            type: "IMAGE",
            source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/4.jpg",
            size: 521
        }, {
            type: "IMAGE",
            source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/5.jpg",
            size: 521
        }, {
            type: "IMAGE",
            source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/6.jpg",
            size: 521
        }, {
            type: "IMAGE",
            source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/7.jpg",
            size: 521
        }, {
            type: "IMAGE",
            source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/4.jpg",
            size: 521
        }, {
            type: "IMAGE",
            source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/loeries2016.jpg",
            size: 521
        }, {
            type: "IMAGE",
            source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/loeries.jpg",
            size: 521
        }, {
            type: "IMAGE",
            source: "https://188.226.164.22/wp-content/themes/shopkeeper/images/2.jpg",
            size: 521
        } ]
    };
}

$.html5Loader({
    filesToLoad: preloaderFiles,
    onBeforeLoad: function() {
        preloader.show();
    },
    onComplete: function() {
        setTimeout(initPage, 1e3);
    },
    onUpdate: function(e) {
        preloaderBar.width(e + "%");
    },
    onMediaError: function(e, t) {
        console.log(e);
    }
}), $(window).resize(resizeWindow), $(window).scroll(function() {
    w.top = $(window).scrollTop(), over_open && !over__clients_ready && w.top > over__clients_top && animateOver();
}), w.device.mobileJquery || $("body").on("mousewheel", function(e) {
    e.deltaY > 0 && screens_ready && !over_open ? (stopScreens(), setScreen(!1, !1)) : e.deltaY < 0 && screens_ready && !over_open && (stopScreens(), 
    setScreen(!0, !1));
}), w.device.mobileJquery || $(document).keyup(function(e) {
    37 !== e.keyCode && 38 !== e.keyCode || !screens_ready || over_open ? 39 !== e.keyCode && 40 !== e.keyCode || !screens_ready || over_open || (stopScreens(), 
    setScreen(!0, !1)) : (stopScreens(), setScreen(!1, !1));
}), w.device.mobileJquery && $("#screen").swipe({
    swipe: function(e, t, n, i, r, o) {
        "up" == t && screens_ready && !over_open ? (stopScreens(), setScreen(!0, !1)) : "down" == t && screens_ready && !over_open && (stopScreens(), 
        setScreen(!1, !1));
    },
    threshold: 75
});
