function getWindowHeight() {
    var e = 0;
    return "number" == typeof window.innerHeight ? e = window.innerHeight : document.documentElement && document.documentElement.clientHeight ? e = document.documentElement.clientHeight : document.body && document.body.clientHeight && (e = document.body.clientHeight), 
    e;
}

$(window).scroll(function() {
    $(this).scrollTop() > 0 ? $("#base_breadcrumb li").fadeOut() : $("#base_breadcrumb li").fadeIn();
}), $(window).load(function() {
    $("#status").delay(1e3).fadeOut(500), $("#preloader").delay(1100).fadeOut("slow"), 
    $("body").delay(1100).css({
        overflow: "visible"
    });
}), $(document).ready(function() {
    function e() {
        return 1 == i ? ($("#base_menu_navigation").animate({
            right: 0
        }), $("#base_menu_icon").animate({
            right: 270
        }), i = 2) : ($("#base_menu_navigation").animate({
            right: -250
        }), $("#base_menu_icon").animate({
            right: 70
        }), i = 1), !1;
    }
    $("#footer").html("Copyright 2017. All Rights Reserved"), $("#view_details").unbind().click(function() {
        $("#job_details,#job_details_overlay").fadeIn("slow");
    }), $("#view_details").one("click", function() {
        $("#adaptive").lightSlider({
            adaptiveHeight: !0,
            item: 1,
            slideMargin: 0,
            loop: !1
        });
    }), $(".close_btn").click(function() {
        $(".content_lightbox").fadeOut("slow");
    });
    var i = 1;
    $("#base_menu_icon").click(function() {
        $(this).toggleClass("btn-open").toggleClass("btn-close");
    }), $(".menu_trigger").on("click", e), $(".has_sub").click(function() {
        $(this).find(".base_menu_subs").slideToggle();
    }), $(".work_wrapper").click(function() {
        $("html, body").animate({
            scrollTop: 0
        }, "slow");
        var e = $("#base_works_reveal"), i = e.height(), t = e.css("height", "auto").height();
        e.height(i).animate({
            height: t
        }, 500);
    }), $(".base_work").hover(function() {
        $(this).find(".og_tile_grid_writeup_overlay").fadeToggle();
    }), $(".base_works_reveal_close").click(function() {
        $("#base_works_reveal").animate({
            height: 0
        });
    });
});
