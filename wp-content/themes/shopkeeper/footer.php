					<?php global $page_id, $woocommerce, $shopkeeper_theme_options; ?>
                    
                    <?php

                    $page_footer_option = "on";
					
					if (get_post_meta( $page_id, 'footer_meta_box_check', true )) {
						$page_footer_option = get_post_meta( $page_id, 'footer_meta_box_check', true );
					}

					if (class_exists('WooCommerce')) {
						if (is_shop() && get_post_meta( get_option( 'woocommerce_shop_page_id' ), 'footer_meta_box_check', true )) {
							$page_footer_option = get_post_meta( get_option( 'woocommerce_shop_page_id' ), 'footer_meta_box_check', true );
						}
					}
					
					?>
					
					<?php if ( $page_footer_option == "on" ) : ?>
                    
                    <div class="footer" style="font-family: NeueEinstellung;">
                        <div class="footer-shadow hidden-print"></div>
                        <div class="footer-container hidden-print">
                            <!--    FOOTER [start]    -->
                            <footer class="footer color1">
                                <div class="visible-xs text-center back-to-top">
                                    <a href="#">Back To Top</a>
                                </div>
                                <div class="text-center need-help typo5 color3">
                                    <span>
                                    <a style="text-decoration: none;">Need help? Contact us </a>
                                </span>
                                    <span class="linkCallExpert">
                                        <a data-telephone="+254726966315" class="callExpertTel">+254 726 966315 / +254 20 760 7222</a>
                                    </span>
                                </div>
                                <div class="container-fluid editorial-max-width">
                                    <div id="footer-menu" class="row content">
                                        <div class="col-md-2 col-sm-12 box">
                                            <div class="parsys col1">
                                                <div class="ghost section">
                                                </div>
                                                <div class="parbase section containerlink">
                                                    <ul>
                                                        <li>
                                                            <a class="title font-family3">Info</a>
                                                            <ul class="submenu typo9">
                                                                <li><a href="#" target="_blank" class="link typo9 color3">News</a></li>
                                                                
                                                               
                                                                
                                                                <li><a href="#" target="_blank" class="link typo9 color3">FAQS</a></li>
                                                                <li><a href="#" target="_blank" class="link typo9 color3">SIZING</a></li>
                                                                
                                                                <li><a href="#" target="_blank" class="link typo9 color3" download>Our Vow</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-md-offset-1 col-sm-12 box">
                                            <div class="parsys col2">
                                                <div class="parbase section containerlink">
                                                    <ul>
                                                        <li>
                                                            <a class="title font-family3">ABOUT US</a>
                                                            <ul class="submenu typo9">
                                                                <li><a href="#" class="link typo9 color3"> OUR STORY</a></li>
                                                                <li><a href="#" class="link typo9 color3">MADE TO MEASURE</a></li>
                                                                <li><a href="#" class="link typo9 color3">CAREERS</a></li>
                                                                <li><a href="#" class="link typo9 color3"></a></li>
                                                                <li><a href="#" class="link typo9 color3"></a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="ghost section">
                                                </div>
                                                <div class="ghost section">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-md-offset-1 col-sm-12 box">
                                            <div class="parsys col3">
                                                <div class="ghost section">
                                                </div>
                                                <div class="parbase section containerlink">
                                                    <ul>
                                                        <li>
                                                            <a class="title font-family3">Customer Care</a>
                                                            <ul class="submenu typo9">
                                                                <li><a href="#" class="link typo9 color3">RETURNS & REFUNDS</a></li>
                                                                <li><a href="#" class="link typo9 color3">PRIVACY POLICY</a></li>
                                                                <li><a href="#" class="link typo9 color3">TRACK YOUR ORDER</a></li>
                                                                <li><a href="#" class="link typo9 color3">CONTACT US</a></li>
                                                                <li><a href="#" class="link typo9 color3"></a></li>
                                                                <li><a href="#" class="link typo9 color3"></a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="ghost section">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-md-offset-1 col-sm-12 box">
                                            <div class="col4 parsys">
                                                <div class="parbase section subscribenewsletter">
                                                    <ul>
                                                        <li>
                                                            <a class="title typo8 color1">Subscribe the Newsletter</a>
                                                            <ul class="submenu">
                                                                <li class="typo11 color3 subscribe-note">Join our email newsletter to get exclusive offers and first access to products</li>
                                                                
                                                                <li class="subscribeBottom newsletter-form-footer">
                                                                    <div class="textbox-submit">
                                                                        <input id="email-newsletter-footer" type="text" value="" placeholder="Enter Email Address" class="subscribe-input textbox" style="padding:25px 15px;">
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <ul style="display:inline-block;text-decoration:none;padding: 10px 0">
                                                                        <li style="display:inline-block;margin-right:10px;"><a href=""><i class="fa fa-2 fa-facebook"></i></a></li>
                                                                        <li style="display:inline-block;margin-right:10px;"><a href=""><i class="fa fa-twitter"></i></a></li>
                                                                        <li style="display:inline-block;margin-right:10px;"><a href=""><i class="fa fa-instagram"></i></a></li> 
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="parbase section containerlink">
                                                    <ul>
                                                        <li>
                                                            <a class="title typo8 color1"></a>
                                                            <ul class="submenu">
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="ghost section">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="bottom">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-sm-4 copy typo11 color2">
                                                2018 &copy; GENTEEL FASHION & GROOMING LTD. <a href="#"></a> 
                                                
                                            </div>
                                            <div class="col-sm-4" style="margin-top:4px;float:right;">
                                                <ul style="display:inline-block;text-decoration:none;padding: 10px 0;height:50px;">
                                                    <li style="display:inline-block;margin-right:10px"><img src="http://genteel.skyetechgroup.com/wp-content/uploads/2017/08/visa-e1502897048787.png" /></li>
                                                    <li style="display:inline-block;margin-right:10px"><img src="http://genteel.skyetechgroup.com/wp-content/uploads/2017/08/paypal-e1502897073126.png" /></li>
                                                    <li style="display:inline-block;margin-right:10px"><img src="" /></li> 
                                                </ul>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </footer>
                        </div>
                        <!-- FOOTER [end] -->
                        
                    </div>
                    
                    <?php endif; ?>
                    
                </div><!-- #page_wrapper -->
                        
            </div><!-- /st-content -->
        </div><!-- /st-pusher -->        
        
        <?php if (class_exists('WooCommerce') && (is_shop() || is_product_category() || is_product_tag() )) : ?>
        <nav class="st-menu slide-from-left <?php echo ( is_active_sidebar( 'catalog-widget-area' ) && ( isset($shopkeeper_theme_options['sidebar_style']) && ( $shopkeeper_theme_options['sidebar_style'] == "0" ) ) ) ? 'hide-for-large-up':''; ?> <?php echo ( is_active_sidebar( 'catalog-widget-area' ) ) ? 'shop-has-sidebar':''; ?>">
            <div class="nano">
                <div class="content">
					
                    <div class="offcanvas_content_left wpb_widgetised_column">
                    
                        <div id="filters-offcanvas">
                            <?php if ( is_active_sidebar( 'catalog-widget-area' ) ) : ?>
                                <?php dynamic_sidebar( 'catalog-widget-area' ); ?>
                            <?php endif; ?>
                        </div>
                    
                    </div>
                    
                </div>
            </div>
        </nav>
    	<?php endif; ?>
        
        <nav class="st-menu slide-from-right">
            <div class="nano">
                <div class="content">
                
                    <div class="offcanvas_content_right">                	
                        
                        <div id="mobiles-menu-offcanvas">
                                
                                <?php if ( (isset($shopkeeper_theme_options['main_header_search_bar'])) && ($shopkeeper_theme_options['main_header_search_bar'] == "1") ) : ?>

                                <div class="mobile-search hide-for-large-up">
									
										<?php
										if (class_exists('WooCommerce')) {
											the_widget( 'WC_Widget_Product_Search', 'title=' );
										} else {
											the_widget( 'WP_Widget_Search', 'title=' );
										}
										?>
										
										<div class="mobile_search_submit">
											<i class="fspk-icon-search"></i>
										</div>
								
                                </div>

                                <?php endif; ?>
                                
	                            <?php if ( (isset($shopkeeper_theme_options['main_header_layout'])) && ( $shopkeeper_theme_options['main_header_layout'] != "2" ) && ( $shopkeeper_theme_options['main_header_layout'] != "22" ) ) : ?>

	                                <nav class="mobile-navigation primary-navigation hide-for-large-up" role="navigation">
	                                <?php 
	                                    wp_nav_menu(array(
	                                        'theme_location'  => 'main-navigation',
	                                        'fallback_cb'     => false,
	                                        'container'       => false,
	                                        'items_wrap'      => '<ul id="%1$s">%3$s</ul>',
	                                    ));
	                                ?>
	                                </nav>
	                                
	                            <?php endif; ?>
	                            
	                            <?php if ( (isset($shopkeeper_theme_options['main_header_layout'])) && ( $shopkeeper_theme_options['main_header_layout'] == "2" || $shopkeeper_theme_options['main_header_layout'] == "22" ) ) : ?>
	                                
	                                <nav class="mobile-navigation hide-for-large-up" role="navigation">
	                                <?php 
	                                    wp_nav_menu(array(
	                                        'theme_location'  => 'centered_header_left_navigation',
	                                        'fallback_cb'     => false,
	                                        'container'       => false,
	                                        'items_wrap'      => '<ul id="%1$s">%3$s</ul>',
	                                    ));
	                                ?>
	                                </nav>
	                                
	                                <nav class="mobile-navigation hide-for-large-up" role="navigation">
	                                <?php 
	                                    wp_nav_menu(array(
	                                        'theme_location'  => 'centered_header_right_navigation',
	                                        'fallback_cb'     => false,
	                                        'container'       => false,
	                                        'items_wrap'      => '<ul id="%1$s">%3$s</ul>',
	                                    ));
	                                ?>
	                                </nav>
	                                
	                            <?php endif; ?>
								
								<?php if ( (isset($shopkeeper_theme_options['main_header_off_canvas'])) && (trim($shopkeeper_theme_options['main_header_off_canvas']) == "1" ) ) : ?>
	                                <nav class="mobile-navigation" role="navigation">
	                                <?php 
	                                    wp_nav_menu(array(
	                                        'theme_location'  => 'secondary_navigation',
	                                        'fallback_cb'     => false,
	                                        'container'       => false,
	                                        'items_wrap'      => '<ul id="%1$s">%3$s</ul>',
	                                    ));
	                                ?>
	                                </nav>
	                            <?php endif; ?>
	                            
	                            <?php						
								$theme_locations  = get_nav_menu_locations();
								if (isset($theme_locations['top-bar-navigation'])) {
									$menu_obj = get_term($theme_locations['top-bar-navigation'], 'nav_menu');
								}
								
								if ( (isset($menu_obj->count) && ($menu_obj->count > 0)) || (is_user_logged_in()) ) {
								?>
								
									<?php if ( (isset($shopkeeper_theme_options['top_bar_switch'])) && ($shopkeeper_theme_options['top_bar_switch'] == "1" ) ) : ?>
	                                    <nav class="mobile-navigation hide-for-large-up" role="navigation">								
	                                    <?php 
	                                        wp_nav_menu(array(
	                                            'theme_location'  => 'top-bar-navigation',
	                                            'fallback_cb'     => false,
	                                            'container'       => false,
	                                            'items_wrap'      => '<ul id="%1$s">%3$s</ul>',
	                                        ));
	                                    ?>
	                                    
	                                    <?php if ( is_user_logged_in() ) { ?>
	                                        <ul><li><a href="<?php echo get_site_url(); ?>/?<?php echo get_option('woocommerce_logout_endpoint'); ?>=true" class="logout_link"><?php _e('Logout', 'woocommerce'); ?></a></li></ul>
	                                    <?php } ?>
	                                    </nav>
	                                <?php endif; ?>
								
								<?php } ?>
								
								<div class="language-and-currency-offcanvas hide-for-large-up">
								
									<?php if (function_exists('icl_get_languages')) { ?>
					                    <?php languages_top_bar(); ?>
									<?php } ?>
									
									<?php if (class_exists('woocommerce_wpml')) { ?>
										<?php echo(do_shortcode('[currency_switcher format="%code% - (%symbol%)"]')); ?>
									<?php } ?>
				
	                            
	                            </div>
                            
                        </div>

                        <?php if ( is_active_sidebar( 'offcanvas-widget-area' ) ) : ?>
                        	<div class="shop_sidebar wpb_widgetised_column">
                            	<?php dynamic_sidebar( 'offcanvas-widget-area' ); ?>
                            </div>
                        <?php endif; ?>
                                            
                    </div>
                
                </div>
            </div>
        </nav>
	
    </div><!-- /st-container -->

    <!-- ******************************************************************** -->
    <!-- * Mini Cart ******************************************************** -->
    <!-- ******************************************************************** -->

    <?php if ( (isset($shopkeeper_theme_options['main_header_shopping_bag'])) && ($shopkeeper_theme_options['main_header_shopping_bag'] == "1") ) : ?>
	    <div class="shopkeeper-mini-cart">
	    	<?php if ( class_exists( 'WC_Widget_Cart' ) ) { the_widget( 'WC_Widget_Cart' ); } ?>

	    	<?php 
	    		if (!empty($shopkeeper_theme_options['main_header_minicart_message'])):
	    			echo '<div class="minicart-message">'.$shopkeeper_theme_options['main_header_minicart_message'].'</div>';
	    		endif;
	    	?>
	    </div>
	<?php endif; ?>

	<!-- ******************************************************************** -->
    <!-- * Site Search ****************************************************** -->
    <!-- ******************************************************************** -->
	<div class="site-search">
		<div class="site-search-inner">
		<?php
		if (class_exists('WooCommerce')) {
			the_widget( 'WC_Widget_Product_Search', 'title=' );
		} else {
			the_widget( 'WP_Widget_Search', 'title=' );
		}
		?>
		</div>
	</div><!-- .site-search -->

	<!-- ******************************************************************** -->
    <!-- * Back To Top Button *********************************************** -->
    <!-- ******************************************************************** -->
    <?php $shopkeeper_theme_options['back_to_top_button'] = isset($shopkeeper_theme_options['back_to_top_button']) ? $shopkeeper_theme_options['back_to_top_button'] : '1'; ?>
	<?php if ( $shopkeeper_theme_options['back_to_top_button'] == '1') : ?>
	<a href="#0" class="cd-top">
		<i class="spk-icon-up-small" aria-hidden="true"></i>

	</a>
	<?php endif; ?>


	<!-- ******************************************************************** -->
    <!-- * Product Quick View *********************************************** -->
    <!-- ******************************************************************** -->
    <!-- <div id="quick_view_container">
		<div id="placeholder_product_quick_view" class="woocommerce"></div>
	</div> -->

	<div class="cd-quick-view woocommerce">
	</div> <!-- cd-quick-view -->

    <!-- ******************************************************************** -->
    <!-- * Custom Footer JavaScript Code ************************************ -->
    <!-- ******************************************************************** -->
    
    <?php if ( (isset($shopkeeper_theme_options['footer_js'])) && ($shopkeeper_theme_options['footer_js'] != "") ) : ?>
		<?php echo $shopkeeper_theme_options['footer_js']; ?>
    <?php endif; ?>
	
    <!-- ******************************************************************** -->
    <!-- * WP Footer() ****************************************************** -->
    <!-- ******************************************************************** -->
	
	<?php wp_footer(); ?>
    
</body>

</html>