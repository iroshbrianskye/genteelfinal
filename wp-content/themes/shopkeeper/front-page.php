<?php /* Template Name: CustomGenteelHomepage */ ?>

<?php get_header('custom'); ?>

<div class="screen" id="screen">
  <div id="footer" class="index_footer">
      Copyright 2017. All Rights Reserved
  </div>
  <div class="screen__item">
    <div class="screen__visual image adweek_2016"></div>
    <div class="screen__content">
      <h2>Suits</h2>
      <div class="issa" style="position: absolute;left:50%;top: 58%;display: inline-block;margin: 25px auto 0;">
          <a class="isuzu" href="http://genteel.skyetechgroup.com/made-to-measure">Book Appointment</a>
      </div>
      <div class="screen__description">
        <h3>FASHION AGENCY OF THE YEAR</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
      </div>
    </div>
  </div>
  <div class="screen__item">
    <div class="screen__visual image campaign_loeries"></div>
    <div class="screen__content">
      <h2>LookBooks</h2>
      <div class="issa" style="position: absolute;left:50%;top: 58%;display: inline-block;margin: 25px auto 0;">
          <a class="isuzu" href="http://genteel.skyetechgroup.com/the-lookbooks/">Explore</a>
      </div>
      <div class="screen__description">
        <h3>Agency of the Year</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
      </div>
    </div>
  </div>
  <div class="screen__item">
    <div class="screen__visual image campaign_apa_2016"></div>
    <div class="screen__content">
      <h2>
        Style Commandments
      </h2>
      <div class="issa" style="position: absolute;left:50%;top: 58%;display: inline-block;margin: 25px auto 0;">
          <a class="isuzu" href="http://genteel.skyetechgroup.com/7-style-commandments/">Explore</a>
      </div>
      <div class="screen__description">
        <h3>FASHION AGENCY OF THE YEAR</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
      </div>
    </div>
  </div>
  <div class="screen__item">
    <div class="screen__visual image campaign_loeries_2016"></div>
    <div class="screen__content">
      <h2>We Are Culture</h2>
      <div class="issa" style="position: absolute;left:50%;top: 58%;display: inline-block;margin: 25px auto 0;">
          <a class="isuzu" href="http://genteel.skyetechgroup.com/cultural-quest">Explore</a>
      </div>
      <div class="screen__description">
        <h3>FASHION AGENCY OF THE YEAR</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
      </div>
    </div>
  </div>
	<div class="screen__item">
		<div class="screen__visual image afropolitans"></div>
		<div class="screen__content">
			<h2>Blog</h2>
      <div class="issa" style="position: absolute;left:50%;top: 58%;display: inline-block;margin: 25px auto 0;">
          <a class="isuzu" href="blog.genteel.co.ke">Get Inspired</a>
      </div>
			<div class="screen__description">
				<h3>We are the creative innovation</h3>
				<p>A time of new age thinking. Where the world can change in a heartbeat, and we’ll always be at the forefront of it.</p>
			</div>
		</div>
	</div>

  <div class="screen__item">
    <div class="screen__visual image billion_reasons"></div>
    <div class="screen__content">
      <h2>Shop</h2>
      <div class="issa" style="position: absolute;left:50%;top: 58%;display: inline-block;margin: 25px auto 0;">
          <a class="isuzu" href="http://genteel.skyetechgroup.com/shop">Buy Now</a>
      </div>
      <div class="screen__description">
        <h3>FASHION AGENCY OF THE YEAR</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
      </div>
    </div>
  </div>

	
	<div class="screen__item">
		<div class="screen__visual image dreamers"></div>
		<div class="screen__content">
			<h2>FPL 2.0</h2>
			<div class="issa" style="position: absolute;left:50%;top: 58%;display: inline-block;margin: 25px auto 0;">
          <a class="isuzu" href="http://genteel.skyetechgroup.com/news">Read More</a>
            </div>
			<div class="screen__description">
				<h3>We are the era of imagination</h3>
				<p>Some people say that all there is to be discovered, has already been discovered. We can prove that's not the case.</p>
			</div>
		</div>
	</div>


</div><!-- /screen -->

<div class="progress" id="progress"></div>
<div class="preloader" id="preloader"><div class="bar"></div><div class="b"></div></div>

 
<?php get_footer('custom'); ?>
