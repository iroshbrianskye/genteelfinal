<!DOCTYPE html>

<!--[if IE 9]>
<html class="ie ie9" <?php language_attributes(); ?>>
<![endif]-->

<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">


    <!-- ******************************************************************** -->
    <!-- * WordPress wp_head() ********************************************** -->
    <!-- ******************************************************************** -->

    <?php wp_head(); ?>
	<style>
		.body2{overflow-y:visible !important; overflow-x:hidden !important}
	</style>
</head>

<body class="body2">

<div id="base_header" class="homepage_header" style="background-color: black; height:20% !important;">
    <div id="base_logo"><a href="index"><img src="http://genteel.skyetechgroup.com/wp-content/uploads/2017/07/Genteel_Wordmark_With_Trademark.png"/></a></div>
    <div id="base_menu_icon" class="btn-open menu_trigger">MENU</div>
    <div id="Mobi_menu" class="menu_trigger mobi_only"></div>
</div>
<div id="base_menu_navigation">
	<div id="base_main_menu">
    	<div class="menu_trigger mobi_only close_menu">Close Menu<span></span></div>
    	<ul>
          <li><a href="#">Home</a></li>
          <li><a href="http://genteel.skyetechgroup.com/about/">About Us</a></li>
          <li><a href="#http://genteel.skyetechgroup.com/shop/" target="_blank">Shop</a></li>
          <li class="has_sub"><a href="#">Custom Made Clothing</a>
          	<ul class="base_menu_subs">
          	    <li><a href="http://genteel.skyetechgroup.com/custom-made-clothing/">Home</a></li>
              	<li><a href="http://genteel.skyetechgroup.com/bespoke/">Bespoke</a></li>
                  <li><a href="http://genteel.skyetechgroup.com/made-to-measure/">Made to Measure</a></li>
              </ul>
          </li>
          <li><a href="http://genteel.skyetechgroup.com/7-style-commandments/">7 Menswear Style Commandments</a></li>
          <li class="has_sub"><a href="#">Cultural Quest</a>
            <ul class="base_menu_subs">
                <li><a href="http://genteel.skyetechgroup.com/cultural-quest/">Home</a></li>
                  <li><a href="http://genteel.skyetechgroup.com/cultural-quest/samburu-culture">Samburu</a></li>
                  <li><a href="#">Maasai</a></li>
              </ul>
          </li>
          
          <li class="has_sub"><a href="#">Look Book</a>
            <ul class="base_menu_subs">
                <li><a href="http://genteel.skyetechgroup.com/lookbook-1/">Look 1</a></li>
                <li><a href="http://genteel.skyetechgroup.com/lookbook-2/">Look 2</a></li>
                <!--<li><a href="#">Look 2</a></li>-->
                <!--<li><a href="#">Look 3</a></li>-->
              </ul>
          </li>
          <li><a href="http://blog.genteel.co.ke">Blog</a></li>
          <li><a href="http://genteel.skyetechgroup.com/news/">News</a></li>
          <li><a href="http://genteel.skyetechgroup.com/contact/">Contact Us</a></li>
        </ul>
    </div>
    <div id="base_copyright">
        COPYRIGHT <br>
        <a href="terms_and_conditions">Terms and Conditions.</a><br>
        FIND US
        <ul id="base_socials">
            <li><a href="https://www.facebook.com/" target="_blank" id="fb_icon"></a></li>
            <li><a href="https://twitter.com/" id="twitter_icon" target="_blank"></a></li>
        </ul>
    </div>
    
</div>



