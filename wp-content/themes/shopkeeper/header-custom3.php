<!DOCTYPE html>
<html itemscope itemtype="" class=" js theme-light" data-model-preset="Ghost">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <script>
    var re = /(menu|models)\.html/
    if (re.test(window.location.href)) {
        window.location.href = window.location.href.replace(re, 'html');
    }
    </script>
    <meta name="title" content="">
    <meta name="description" content="Experience a masterpiece of simplicity.">
    <meta name="keywords">
    <meta property="og:title" content="">
    <meta property="og:description" content="Experience a masterpiece of simplicity.">
    <meta property="og:url" content="">
    <meta property="og:image" content="images/10.EWBShanghai_16x9_fullbleed-image.jpg">
    <meta property="og:image:secure_url" content="images/10.EWBShanghai_16x9_fullbleed-image.jpg">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="Experience a masterpiece of simplicity.">
    <meta name="twitter:url" content="">
    <meta name="twitter:image" content="images/10.EWBShanghai_16x9_fullbleed-image.jpg">
    <meta itemprop="name" content="">
    <meta itemprop="description" content="Experience a masterpiece of simplicity.">
    <meta itemprop="image" content="images/10.EWBShanghai_16x9_fullbleed-image.jpg">
   
     <!-- ******************************************************************** -->
    <!-- * WordPress wp_head() ********************************************** -->
    <!-- ******************************************************************** -->

    <?php wp_head(); ?>
</head>

<body>


			 <?php

					$header_sticky_class = "";
					$header_transparency_class = "";
					$transparency_scheme = "";
					
					if ( (isset($shopkeeper_theme_options['sticky_header'])) && ($shopkeeper_theme_options['sticky_header'] == "1" ) ) {
						$header_sticky_class = "sticky_header";
					}
					
					if ( (isset($shopkeeper_theme_options['main_header_transparency'])) && ($shopkeeper_theme_options['main_header_transparency'] == "1" ) ) {
						$header_transparency_class = "transparent_header";
					}
					
					if ( (isset($shopkeeper_theme_options['main_header_transparency_scheme'])) ) {
						$transparency_scheme = $shopkeeper_theme_options['main_header_transparency_scheme'];
					}
					
					$page_id = "";
					if ( is_single() || is_page() ) {
						$page_id = get_the_ID();
					} else if ( is_home() ) {
						$page_id = get_option('page_for_posts');		
					}
					
					if ( (get_post_meta($page_id, 'page_header_transparency', true)) && (get_post_meta($page_id, 'page_header_transparency', true) != "inherit") ) {
						$header_transparency_class = "transparent_header";
						$transparency_scheme = get_post_meta( $page_id, 'page_header_transparency', true );
					}
					
					if ( (get_post_meta($page_id, 'page_header_transparency', true)) && (get_post_meta($page_id, 'page_header_transparency', true) == "no_transparency") ) {
						$header_transparency_class = "";
						$transparency_scheme = "";
					}

					if (class_exists('WooCommerce')) 
                    {
                        if ( is_product_category() && is_woocommerce() )
                        {
                        	if ( $shopkeeper_theme_options['shop_category_header_transparency_scheme'] == 'inherit' )
                        	{
                        		// do nothing, inherit
                        	}
                        	else if ( $shopkeeper_theme_options['shop_category_header_transparency_scheme'] == 'no_transparency' )
                        	{
                        		$header_transparency_class = "";
								$transparency_scheme = "";
                        	}
                        	else 
                        	{
	                            $header_transparency_class = "transparent_header";
	                            $transparency_scheme = $shopkeeper_theme_options['shop_category_header_transparency_scheme'];
                        	}
                        }
                    }
					
					/*if ( is_shop() ) {
						$header_transparency_class = "";
					}*/
					
					?>

					<div id="page_wrapper" class="<?php echo $header_sticky_class; ?> <?php echo $header_transparency_class; ?> <?php echo $transparency_scheme; ?>">
                    
                        <?php do_action( 'before' ); ?>                     
                        
                        <?php
    
						$header_max_width_style = "100%";
						if ( (isset($shopkeeper_theme_options['header_width'])) && ($shopkeeper_theme_options['header_width'] == "custom") ) {
							$header_max_width_style = $shopkeeper_theme_options['header_max_width']."px";
						} else {
							$header_max_width_style = "100%";
						}
						
						?>
                        
                        <div class="top-headers-wrapper">
						
                            <?php if ( (isset($shopkeeper_theme_options['top_bar_switch'])) && ($shopkeeper_theme_options['top_bar_switch'] == "1" ) ) : ?>                        
                                <?php include(locate_template('header-topbar.php')); ?>						
                            <?php endif; ?>
                            
                            <?php if ( isset($shopkeeper_theme_options['main_header_layout']) ) : ?>
								
								<?php if ( $shopkeeper_theme_options['main_header_layout'] == "1" || $shopkeeper_theme_options['main_header_layout'] == "11" ) : ?>
									<?php include(locate_template('header-default.php')); ?>
                                <?php elseif ( $shopkeeper_theme_options['main_header_layout'] == "2" || $shopkeeper_theme_options['main_header_layout'] == "22" ) : ?>
                                	<?php include(locate_template('header-centered-2menus.php')); ?>
                                <?php elseif ( $shopkeeper_theme_options['main_header_layout'] == "3" ) : ?>
                                	<?php include(locate_template('header-centered-menu-under.php')); ?>
								<?php endif; ?>
                                
                            <?php else : ?>
                            
                            	<?php include(locate_template('header-default.php')); ?>
                            
                            <?php endif; ?>
                        
                        </div>
