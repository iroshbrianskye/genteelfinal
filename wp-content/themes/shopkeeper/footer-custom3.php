                       <div class="footer" style="font-family: NeueEinstellung;">
                        <div class="footer-shadow hidden-print"></div>
                        <div class="footer-container hidden-print">
                            <!--    FOOTER [start]    -->
                            <footer class="footer color1">
                                <div class="visible-xs text-center back-to-top">
                                    <a href="#">Back To Top</a>
                                </div>
                                <div class="text-center need-help typo5 color3">
                                    <span>
                                    <a style="text-decoration: none;">Need help? Contact us </a>
                                </span>
                                    <span class="linkCallExpert">
                                        <a data-telephone="+254726966315" class="callExpertTel">+254 726 966315 / +254 20 760 7222</a>
                                    </span>
                                </div>
                                <div class="container-fluid editorial-max-width">
                                    <div id="footer-menu" class="row content">
                                        <div class="col-md-2 col-sm-12 box">
                                            <div class="parsys col1">
                                                <div class="ghost section">
                                                </div>
                                                <div class="parbase section containerlink">
                                                    <ul>
                                                        <li>
                                                            <a class="title font-family3">Info</a>
                                                            <ul class="submenu typo9">
                                                                <li><a href="#" target="_blank" class="link typo9 color3">News</a></li>
                                                                <li><a href="#" target="_blank" class="link typo9 color3">Contact Us</a></li>
                                                                <li><a href="#" target="_blank" class="link typo9 color3"> </a></li>
                                                                
                                                                <li><a href="#" target="_blank" class="link typo9 color3">FAQS</a></li>
                                                                <li><a href="#" target="_blank" class="link typo9 color3">SIZING</a></li>
                                                                <li><a href="#" target="_blank" class="link typo9 color3">SHIPPING</a></li>
                                                                <li><a href="#" target="_blank" class="link typo9 color3" download>Our Vow</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-md-offset-1 col-sm-12 box">
                                            <div class="parsys col2">
                                                <div class="parbase section containerlink">
                                                    <ul>
                                                        <li>
                                                            <a class="title font-family3">ABOUT US</a>
                                                            <ul class="submenu typo9">
                                                                <li><a href="#" class="link typo9 color3"> OUR STORY</a></li>
                                                                <li><a href="#" class="link typo9 color3">MADE TO MEASURE</a></li>
                                                                <li><a href="#" class="link typo9 color3">CAREERS</a></li>
                                                                <li><a href="#" class="link typo9 color3"></a></li>
                                                                <li><a href="#" class="link typo9 color3"></a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="ghost section">
                                                </div>
                                                <div class="ghost section">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-md-offset-1 col-sm-12 box">
                                            <div class="parsys col3">
                                                <div class="ghost section">
                                                </div>
                                                <div class="parbase section containerlink">
                                                    <ul>
                                                        <li>
                                                            <a class="title font-family3">Customer Care</a>
                                                            <ul class="submenu typo9">
                                                                <li><a href="#" class="link typo9 color3">RETURNS & REFUNDS</a></li>
                                                                <li><a href="#" class="link typo9 color3">PRIVACY POLICY</a></li>
                                                                <li><a href="#" class="link typo9 color3">TRACK YOUR ORDER</a></li>
                                                                <li><a href="#" class="link typo9 color3">CONTACT US</a></li>
                                                                <li><a href="#" class="link typo9 color3"></a></li>
                                                                <li><a href="#" class="link typo9 color3"></a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="ghost section">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-md-offset-1 col-sm-12 box">
                                            <div class="col4 parsys">
                                                <div class="parbase section subscribenewsletter">
                                                    <ul>
                                                        <li>
                                                            <a class="title typo8 color1">Subscribe the Newsletter</a>
                                                            <ul class="submenu">
                                                                <li class="typo11 color3 subscribe-note">Join our email newsletter to get exclusive offers and first access to products</li>
                                                                
                                                                <li class="subscribeBottom newsletter-form-footer">
                                                                    <div class="textbox-submit">
                                                                        <input id="email-newsletter-footer" type="text" value="" placeholder="Enter Email Address" class="subscribe-input textbox" style="padding:25px 15px;">
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <ul style="display:inline-block;text-decoration:none;padding: 10px 0">
                                                                        <li style="display:inline-block;margin-right:10px;"><a href=""><i class="fa fa-2 fa-facebook"></i></a></li>
                                                                        <li style="display:inline-block;margin-right:10px;"><a href=""><i class="fa fa-twitter"></i></a></li>
                                                                        <li style="display:inline-block;margin-right:10px;"><a href=""><i class="fa fa-instagram"></i></a></li> 
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="parbase section containerlink">
                                                    <ul>
                                                        <li>
                                                            <a class="title typo8 color1"></a>
                                                            <ul class="submenu">
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="ghost section">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="bottom">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-sm-4 copy typo11 color2">
                                                2018 &copy; GENTEEL FASHION & GROOMING LTD. <a href="#"></a> 
                                                
                                            </div>
                                            <div class="col-sm-4" style="margin-top:4px;position: absolute;right: 0%;">
                                                <ul style="display:inline-block;text-decoration:none;padding: 10px 0;height:50px;">
                                                    <li style="display:inline-block;margin-right:10px"><img src="http://genteel.skyetechgroup.com/wp-content/uploads/2017/08/visa-e1502897048787.png" /></li>
                                                    <li style="display:inline-block;margin-right:10px"><img src="http://genteel.skyetechgroup.com/wp-content/uploads/2017/08/paypal-e1502897073126.png" /></li>
                                                    <li style="display:inline-block;margin-right:10px"><img src="" /></li> 
                                                </ul>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                          


