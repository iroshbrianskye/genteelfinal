<?php

// add_theme_support( 'header-custom' );
// add_theme_support( 'header-custom2' );
// add_theme_support( 'header-custom3' );
// add_theme_support( 'footer-custom' );
// add_theme_support( 'footer-custom2' );
// add_theme_support( 'footer-custom3' );


// /******************************************************************************/
// /**************************** Enqueue styles **********************************/
// /******************************************************************************/

// /******************************************************************************/
// /**************************** Custom Genteel styles **********************************/
// /******************************************************************************/

// function add_bespoke_scripts() {
// 	 if( is_page_template( 'about-us.php' ) ) {

//           wp_enqueue_style( 'about.min', get_template_directory_uri() . '/css/genteel/about.min.css');
//           wp_enqueue_style( 'cust-footer', get_template_directory_uri() . '/css/genteel/cust-footer.css');

//           wp_enqueue_script( 'jquery.min', get_template_directory_uri() . '/js/components/jquery.min.js', NULL,NULL, false);
//           wp_enqueue_script( 'combined.min', get_template_directory_uri() . '/js/genteel/combined.min.js', array ( 'jquery' ), NULL,false);
//           wp_enqueue_script( 'jquery.blockUI', get_template_directory_uri() . '/js/genteel/jquery.blockUI.js', array ( 'jquery' ), NULL,true);
// 	      wp_enqueue_script( 'about.min', get_template_directory_uri() . '/js/genteel/about.min.js', array ( 'jquery' ), NULL,false);

//         }

//  	if( is_page_template( 'bespoke.php' ) ) {

//           wp_enqueue_style( 'ghost', get_template_directory_uri() . '/css/genteel/ghost.css');

//           wp_enqueue_script( 'jquery.min', get_template_directory_uri() . '/js/components/jquery.min.js', NULL,NULL, false);
//           wp_enqueue_script( 'ghost', get_template_directory_uri() . '/js/genteel/ghost.js', array ( 'jquery' ), NULL,true);
//           wp_enqueue_script( 'bowser', get_template_directory_uri() . '/js/genteel/bowser.min.js', array ( 'jquery' ), NULL,true);

//         }

//     if( is_page_template( 'bespoke-experience.php' ) ) {

//           wp_enqueue_style( 'ghost', get_template_directory_uri() . '/css/genteel/ghost.css');

//           wp_enqueue_script( 'jquery.min', get_template_directory_uri() . '/js/components/jquery.min.js', NULL,NULL, false);
//           wp_enqueue_script( 'ghost', get_template_directory_uri() . '/js/genteel/ghost.js', array ( 'jquery' ), NULL,true);
//           wp_enqueue_script( 'bowser', get_template_directory_uri() . '/js/genteel/bowser.min.js', array ( 'jquery' ), NULL,true);
//         }

// 	if( is_page_template( 'page-full-width.php' ) ) {
// 		 wp_enqueue_style( 'cust-footer', get_template_directory_uri() . '/css/genteel/cust-footer.css');
// 		}

// 	if( is_page( '1289' ) ) {
//           wp_enqueue_style( 'ghost', get_template_directory_uri() . '/css/genteel/ghost.css');
//         }

// 	//if( is_page( '2701') ) {
// 	//  wp_enqueue_style('bootstrap.min', get_template_directory_uri() . '/css/bootstrap.min.css');
// 	//}
//         //if( is_page( '2860') ) {
//         //  wp_enqueue_style('bootstrap.min', get_template_directory_uri() . '/css/bootstrap.min.css');
//         //}

// }
// add_action( 'wp_enqueue_scripts', 'add_bespoke_scripts', 9999 );



// function add_homepage_scripts() {

//  	if( is_page( '2222' ) ) {

// 	  wp_enqueue_style( 'template.min', get_template_directory_uri() . '/css/genteel/template.min.css');
// 	  wp_enqueue_style( 'homepage.min', get_template_directory_uri() . '/css/genteel/homepage.min.css');

// 	  wp_enqueue_script( 'jquery.min', get_template_directory_uri() . '/js/components/jquery.min.js', NULL,NULL, false);
// 	  wp_enqueue_script( 'jquery-base-includes', get_template_directory_uri() . '/js/genteel/jquery-base-includes.js', array ( 'jquery' ), NULL,false);
// 	  wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/genteel/modernizr.js', array ( 'jquery' ), NULL,false);
// 	  wp_enqueue_script( 'homepage', get_template_directory_uri() . '/js/genteel/homepage.js', array ( 'jquery' ),NULL, true);
// 	}

// }
// add_action( 'wp_enqueue_scripts', 'add_homepage_scripts', 999 );


